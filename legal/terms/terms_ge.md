
# Allgemeine Geschäftsbedingungen

Stand: 01.04.2022

## I. Geltungsbereich

1. Die HintHub GmbH, Hauptstr. 28, 15806 Zossen (nachfolgend „HintHub“) betreibt eine Whistleblower Software-as-a-Service-Plattform mit dem Namen HintHub, die es Geschäftskund:innen und öffentlichen Institutionen ermöglicht, unter anderem der Whistleblowerrichtliche (EU) 2019/1937 gerecht zu werden.
2. Dieses Dokument, nachfolgend auch „Allgemeine Geschäftsbedingungen“ oder „AGB“ genannt, regelt die vertraglichen Bedingungen für die Nutzung der HintHub Whistleblower Software-Plattform für alle Nutzer:innen, gleichgültig, ob eine entgeltfreie oder kostenpflichtige Variante der Software zum Einsatz kommt.
3. Es handelt sich um eine „Software as a Service“ (SaaS), die über eine webbasierte Schnittstelle betrieben wird und es juristische Personen und ihren Mitarbeitenden ermöglicht, mit anderen Mitarbeitenden oder Geschäftspartner:innen über die Lösung zu kommunizieren, ohne eine andere Software als einen Internetbrowser zu verwenden.
4. Diese AGB gelten ausschließlich. Abweichende, entgegenstehende oder ergänzende Allgemeine Geschäftsbedingungen der Kund:innen werden nur Vertragsbestandteil, soweit HintHub ihrer Geltung ausdrücklich in Schriftform zugestimmt hat. Dieses Zustimmungserfordernis gilt auch dann, wenn HintHub in Kenntnis der Allgemeinen Geschäftsbedingungen einer anderen Partei vorbehaltlos mit der Leistungsausführung beginnt.

## II. Definitionen

1. “Nutzer:innen“ bezeichnet die Gesamtheit der natürlichen Personen, die als Mitarbeitende oder anderweitig Befähigte bei privatwirtschaftlich organisierten, juristischen Personen oder bei öffentlichen Stellen als diejenigen Personen auftreten, die die HintHub Whistleblower Software-Plattform bedienen.
2. “Kund:in” bezeichnet die Gesamtheit der juristischen Personen privaten oder öffentlichen Rechts, die HintHub gegenüber als Vertragspartner zur Nutzung der HintHub Whistleblower Software-Plattform auftreten.
3. “Lizenznehmer:in” bezeichnet die Teilmenge der Kund:innen, die eine Variante der Software nutzen, die entgeltpflichtig ist, folglich Lizenzgebühren entrichtet.
4. „Bedingungen“ bezeichnet die Bedingungen, die in diesem Dokument enthalten sind.
5. „Einstellungen“ bezeichnet den Abschnitt im HintHub-Portal, in dem Nutzer:innen Angaben zur Kund:in, insb. zu ihrer aktuellen Mitarbeiterzahl und ihren Kontaktdaten machen kann.
6. „Update“ bezeichnet Verbesserungen und Funktionserweiterungen und Updates werden als Softwareaktualisierungen installiert.
7. „User Backend“ bezeichnet den Bereich der Lösung, in dem Nutzer:innen Hinweise sehen bzw. bearbeiten und die Lösung ggf. nach den spezifischen Anforderungen der jeweiligen Organisationen anpassen können. Ob eine Anpassbarkeit möglich ist, hängt davon ab, ob es sich um eine kostenlose Nutzung oder ein kostenpflichtiges Abonnement handelt.
8. „Lizenzgebühr“ bezeichnet jene Gebühr, die die Lizenznehmer:in für das Lösungsnutzungsrecht innerhalb eines Lizenzzeitraums entrichtet.
9. „Lizenzperiode“ ist der Zeitraum, für den die Lizenznehmer:in aufgrund der Zahlung der Lizenzgebühr das Nutzungsrecht gegen Zahlung der Gebühr erhält.
10. “Lizenztypen” bezeichnet die Gesamtheit der Varianten der Software, also eine kostenfreie und eine oder mehrere entgeltpflichtige Sammlungen von Funktionen, an denen Nutzer:innen, Kund:innen und Lizenznehmer:innen ein Nutzungsrecht begründen können.
11. „HintHub-Portal“ oder “Lösung” ist die Anwendung HintHub, die von der Hinthub GmbH entwickelt wurde und zu der die Lizenznehmer:in nach diesen Bedingungen Lösungsnutzungsrechte gewährt werden.
12. „Nutzungsrecht“ bezeichnet das Recht der Lizenznehmer:in, die Lösung gemäß den Bedingungen zu nutzen.
13. „Nutzungsvertrag“ ist die Vereinbarung von HintHub und der Lizenznehmer:in über die Nutzung von HintHub.
14. „Partei“ oder „Parteien“ bezeichnet die Vertragsparteien des Nutzungsvertrags, also Lizenznehmer:in und HintHub, die diesen Bedingungen sowie den im folgenden beschriebenen Rechten und Auflagen unterliegen.
15. „Paket“ oder “Variante” bezeichnet eine Sammlung von Funktionen der Lösung. Die Pakete variieren in Ihrem Leistungsumfang.
16. „Testperiode“ bezeichnet einen bei erster Registrierung am HintHub-Portal beginnenden und in seiner Laufzeit begrenzten Zeitraum. 
17. „Unternehmenseinstellungen“, “Company Profile” oder “Unternehmensprofil” bezeichnet den Abschnitt in der Lösung, in dem die Lizenznehmer:in ihr aktuell gültiges Paket einsehen, das Paket ändern, Zusatzleistungen buchen, Rechnungen herunterladen und Kontakt- und Abrechnungsinformationen, usw. pflegen kann.
18. „Services“ sind die Leistungen, die HintHub der Lizenznehmer:in durch die Bereitstellung von Lösungsnutzungsrechten zur Verfügung stellt.
19. “Leistungsbeschreibung” definiert den Umfang von einzelnen Teilaspekten der Software, die sich je nach gewähltem Paket unterscheiden. Eine aktuelle Übersicht über den Umfang der jeweiligen Pakete und den jeweils zugehörigen Preise finden sich unter [hinthub.eu/plans](https://www.hinthub.eu/de/plans).
20. „Vertragsbeendigung“ ist der Zeitpunkt, an dem eine schriftlich ausgesprochene Kündigung einer Partei unter Einhaltung der in diesem Dokument näher beschriebenen Kündigungsrechte und damit eventuell einhergehender Fristen wirksam wird.
21. „Zusatzleistungen“ sind die Leistungen, die HintHub Lizenznehmern über die Pakete hinaus entgeltlich mit der Buchung zur Verfügung stellt.
22. „Vertrauliche Informationen“ sind sämtliche Informationen, die HintHub oder der Kunde gegen unbeschränkte Weitergabe an Dritte schützen oder die nach den Umständen der Weitergabe oder ihrem Inhalt nach als vertraulich anzusehen sind. Jedenfalls gelten folgende Informationen als Vertrauliche Informationen von HintHub: Sämtliche Software, Programme, Werkzeuge, Preise, Daten oder andere Materialien, die HintHub den Nutzer:innen, Kund:innen oder Lizenznehmer:innen vorvertraglich oder auf Grundlage des Vertrages zur Verfügung stellt.
23. “Ende-zu-Ende-Verschlüsselung” ist eine in der Lösung integrierte Technologie, die die Kommunikation zwischen Nutzer:innen, Kund:innen und Lizenznehmer:innen und Hinweisgebenden so verschlüsselt, dass nur Sender:in und Empfänger:in einer Nachricht diese entschlüsseln und somit in Klartext lesen kann. HintHub ist es durch diese Technologie zu keiner Zeit möglich, die Inhalte der Kommunikation zu lesen oder zu entschlüsseln.

## III. Allgemeine Bedingungen

### A. Geheimhaltung

1. Die Parteien verpflichten sich, alle vor und im Rahmen der Vertragserfüllung erlangten vertraulichen Informationen der jeweils anderen Partei zeitlich unbegrenzt, so, wie sie eigene vergleichbare vertrauliche Informationen schützen, zu schützen, mindestens jedoch mit angemessener Sorgfalt vertraulich zu behandeln.
2. HintHub kann die während der Laufzeit gesammelten Daten, die unter vertrauliche Informationen fallen, in aggregierter, anonymisierter Form verwenden, vorausgesetzt, dass diese Daten von mehr als einer Nutzer:in, Kund:in oder Lizenznehmer:in gesammelt werden und die Mitarbeitenden der jeweiligen Gesellschaften oder Institutionen nicht identifizieren.
3. Die vorstehendenden Ziffern gelten nicht für vertrauliche Informationen, die 
3.1. vom Empfänger ohne Rückgriff auf die vertraulichen Informationen der offenlegenden Partei unabhängig entwickelt worden sind;
3.2. ohne Vertragsverletzung durch den Empfänger allgemein öffentlich zugänglich geworden sind oder rechtmäßig ohne Pflicht zur Geheimhaltung von einem Dritten erhalten wurden, der berechtigt ist, diese Vertraulichen Informationen bereitzustellen;
3.3. dem Empfänger zum Zeitpunkt der Offenlegung ohne Einschränkung bekannt waren;
3.4. nach schriftlicher Zustimmung der offenlegenden Partei von den vorstehenden Regelungen freigestellt sind; oder
3.5. von einem zuständigen Gericht oder Behörde sowie einer zwingenden Vorschrift verlangt werden.
4. HintHub ist berechtigt, z.B. in einer Referenzliste, als Darstellung auf der HintHub Website oder in Newslettern zu veröffentlichen, dass Nutzer:innen und deren Organisationen zum Kundenportfolio von HintHub gehören.

### B. Lizenztypen, Laufzeiten

1. Das Paket wird von einer von der Lizenznehmer:in autorisierten Nutzer:in für die Lizenznehmer:in ausgewählt, wenn sie sich für die Lösung anmeldet und kann für die Zukunft während des Abonnements geändert werden. Die Wahl eines höherwertigen Pakets mit größerem Leistungsumfang ist jederzeit möglich, eine Herabstufung nur zum Ablauf der Lizenzperiode. Ausgenommen hiervon sind etwaige Testperioden, die direkt nach der Registrierung angeboten werden. Im Rahmen dieser können Kund:innen für einen begrenzten Zeitraum entgeltfrei den Funktionsumfang eines generell kostenpflichtigen Pakets nutzen. Zum Ende der Periode müssen sich die Kund:innen für die Fortführung eines höherwertigen Pakets aktiv entscheiden oder sich im Funktionsumfang auf die kostenfreie Variante zurückstufen lassen.
2. Die erste Lizenzperiode läuft 12 Monate. Am Ende jeder Lizenzperiode beginnt automatisch eine neue Lizenzperiode, außer dieser Vertrag wurde gemäß Abschnitt III.F. gekündigt. Die zweite und jede folgende Lizenzperiode läuft ebenfalls 12 Monate, insofern die Lizenznehmer:in nicht einen rabattierten Zeitraum mit einer abweichenden Laufzeit von HintHub angeboten bekommt. Die erste Lizenzperiode beginnt an dem Tag, an dem die Lizenznehmer:in die erste Lizenzgebühr im Hinthub-Portal bezahlt hat.

### C. Testzeiträume

1. Die Länge der Laufzeit von Testzeiträumen wird der Nutzer:in bei der ersten Registrierung angezeigt. Innerhalb dieses Zeitraums stehen der Nutzer:in entgeltfrei Funktionen zur Verfügung, die teilweise oder vollständig einem höherwertigen Paket zuzuordnen sind. Nach Ende des Zeitraums muss sich die Nutzer:in zwingend entscheiden, ob sie den Funktionsumfang kostenpflichtig beibehalten möchte oder auf ein niedrigerwertiges Paket wechselt. Eine detaillierte Darstellung des Leistungsumfangs ist in der Leistungsbeschreibung definiert, die untere [hinthub.eu/plans](https://www.hinthub.eu/de/plans) zu finden ist.
2. Ein Anspruch auf diesen Zeitraum als solches und auf einen bestimmten Funktionsumfang innerhalb der Laufzeit des Testzeitraums besteht nicht. HintHub ist zu jeder Zeit berechtigt, die in diesem Zeitraum zur Verfügung gestellten Funktionen teilweise oder vollständig zu entziehen. Trifft die Nutzer:in am Ende des Zeitraums keine Entscheidung für ein bestimmtes Paket, wird sie auf die kostenfreie Variante heruntergestuft. Wählt die Nutzer:in ein kostenpflichtiges Paket beginnt mit Zahlung die erste Lizenzperiode.

### D. Informationen über Nutzer:innen, Kund:innen und Lizenznehmer:innen; Mitteilungen

1. Nutzer:innen halten für Kund:innen oder Lizenznehmer:innen die Informationen über das Unternehmen oder die Behörde, Mitarbeitendenzahl und Kontaktinformationen in der Lösung aktuell. Nutzer:innen, die für Lizenznehmer:innen handeln, tun dies zudem mit den Zahlungs- und Rechnungsinformationen.
2. Alle Benachrichtigungen, Mitteilungen, Rechnungen usw. werden von HintHub an die Nutzer:innen, Kund:innen oder Lizenznehmer:innen an die im Portal im Abschnitt „Einstellungen“ hinterlegte E-Mail-Adresse gesendet.

### E. Allgemeine Gewährleistung

1. HintHub gibt im Hinblick auf die Erbringung der Services keine Zusicherungen oder Garantien.
2. Im Falle einer mangelhaften Leistung von HintHub gibt die Lizenznehmer:in HintHub Gelegenheit zu mindestens zweimaliger Nacherfüllung innerhalb angemessener Fristen, sofern dies nicht im Einzelfall unzumutbar ist oder besondere Umstände vorliegen, die unter Abwägung der beiderseitigen Interessen eine Auftragsbeendigung durch die Lizenznehmer:in rechtfertigen. HintHub kann nach eigener Wahl den Mangel beseitigen oder die Leistung nochmals mangelfrei erbringen. Schlägt die Nacherfüllung fehl, hat die Lizenznehmer:in unter den gesetzlichen Voraussetzungen das Recht, die Vergütung zu mindern oder vom Vertrag zurückzutreten. Schadenersatzansprüche bestehen nur nach Maßgabe dieser Vertragsbedingungen. Schadenersatzansprüche bestehen jedoch nicht, wenn die Abweichung von der geschuldeten Beschaffenheit nur unerheblich ist.

### F. Haftung

1. In allen Fällen vertraglicher und außervertraglicher Haftung leistet HintHub Schadensersatz oder Ersatz vergeblicher Aufwendungen nur in dem nachfolgend bestimmten Umfang:
1.1. Bei einer leicht fahrlässig verursachten Verletzung wesentlicher Vertragspflichten haftet HintHub außer in den Fällen der Ziffer E.1 oder E.2 der Höhe nach begrenzt auf den vertragstypisch vorhersehbaren Schaden. Wesentliche Vertragspflichten sind abstrakt solche Pflichten, deren Erfüllung die ordnungsgemäße Durchführung eines Vertrages überhaupt erst ermöglicht und auf deren Einhaltung die Vertragsparteien regelmäßig vertrauen dürfen. Im Übrigen ist eine Haftung von HintHub ausgeschlossen. Die Haftung ist für jeden einzelnen Schadensfall und für alle Schadensfälle zusammen aus oder im Zusammenhang mit dem Vertragsverhältnis auf den fünffachen Gesamtbetrag der Vergütung der Lizenznehmer:in für den jeweiligen Service in dem jeweiligen Kalenderjahr beschränkt.
1.2. HintHub haftet nicht für Bedingungen, die außerhalb der Gewalt von HintHub liegen und nicht vorhersehbar waren, wie z.B. Generalstreik, Krieg, Erdbeben, Naturkatastrophen, Streik, Aussperrung und dergleichen.
1.3. Für alle Ansprüche gegen HintHub auf Schadensersatz oder Ersatz vergeblicher Aufwendungen bei vertraglicher und außervertraglicher Haftung gilt eine Verjährungsfrist von einem Jahr. Die Verjährungsfrist beginnt mit dem gesetzlich bestimmten Zeitpunkt (in Deutschland: §199 Abs 1 BGB). Sie tritt spätestens mit Ablauf von fünf Jahren ab Entstehung des Anspruchs ein. Dies gilt nicht (i) bei einer Haftung für Vorsatz oder grober Fahrlässigkeit, (ii) bei Personenschäden, oder (iii) nach dem Produkthaftungsgesetz. Die abweichende Verjährungsfrist für Ansprüche wegen Sach- und Rechtsmängeln bleibt von den Regelungen dieses Absatzes unberührt.

### G. Kündigung

1. Der Service wird der Nutzer:in, der Kund:in und der Lizenznehmer:in bis zur Vertragsbeendigung durch HintHub gemäß Ziffer IV.H zur Verfügung gestellt. HintHub ist berechtigt, den Service mit einer Frist von 12 Monaten zum Ende eines Kalendermonats zu kündigen.
2. Darüber hinaus ist HintHub jederzeit berechtigt, den Service sofort und ohne weitere Benachrichtigung zu kündigen, wenn die Nutzung der Lösung durch die Lizenznehmer:in gegen die Bestimmungen in Abschnitt IV.A. dieser Bedingungen verstößt.
3. Die Lizenznehmer:in kann den Service jederzeit kündigen, indem sie sich im Abschnitt „Unternehmenseinstellungen“ bzw. „Company Profile“ am HintHub-Portal abmeldet. Die Vertragsbeendigung wird am Ende des letzten Tages der aktuellen Lizenzperiode wirksam.
4. Hat die Lizenznehmer:in die Lizenzgebühr 30 Tage nach Beginn einer Lizenzperiode nicht bezahlt, ist HintHub berechtigt, den Service bis zum Eingang einer Zahlung zu pausieren, in dieser Zeit können Hinweise von außen eingehen, aber der Lizenznehmer hat keinen Zugriff auf diese Eingänge. Zahlt der Lizenznehmer die Lizenzgebühr auch nicht nach einer Mahnung binnen, beginnt eine weitere Frist von 14 Tagen, in der der Lizenznehmer:in die Möglichkeit zur Begleichung der säumigen Gebühren eingeräumt wird. Nach Ablauf dieser Frist und der weiteren Säumigkeit, ist HintHub zur Kündigung des Service und Löschung aller Daten auf dem HintHub-Portal berechtigt (vgl. Ziffer IV.D.9).
5. Eine vorausbezahlte Lizenzgebühr wird nicht an die Lizenznehmer:in zurückgezahlt.
6. HintHub ist berechtigt, die Lösung der Lizenznehmer:in und die in der Lösung gespeicherten Daten nach der Leistungserbringung innerhalb des Rahmens der DSGVO zu löschen.
7. Für die Nutzer:innen ist es möglich, die in der Lösung angekommenen Hinweise von Hinweisgeber:innen sowie eine Auflistung der Änderungen an diesen Hinweisen zu exportieren. Möchte die Nutzer:in, die Kund:in oder die Lizenznehmer:in Daten im Falle einer bevorstehenden Vertragsbeendigung aus der Lösung exportieren, so hat die Lizenznehmer:in den Datenexport spätestens am letzten Tag der Lizenzperiode durchzuführen. Wenn die Lizenznehmer:in den Datenexport nicht rechtzeitig durchführt, werden die Daten spätestens nach sechs Monaten gelöscht und der Export der Daten ist nicht mehr möglich.

### H. Pflichtverletzung

1. Jede Partei kann sich auf eine Verletzung berufen, wenn die andere Partei ihre Verpflichtungen gemäß diesen Bedingungen nicht erfüllt. Bevor sie jedoch berechtigt ist, sich auf eine Verletzung zu berufen, muss die Partei, die sich auf die Verletzung beruft, der säumigen Partei eine Frist von sieben Werktagen setzen, um die Verletzung zu beheben. In der Mitteilung ist zu beschreiben, welche Verpflichtungen als verletzt angesehen werden.
2. Wenn der Verstoß erheblich ist, kann die sich hierauf berufende Partei diesen Vertrag kündigen. In diesem Fall muss in der Mitteilung angegeben werden, dass die sich hierauf berufende Partei beabsichtigt, den Service zu beenden.
3. Ein Verstoß bei die Nichtzahlung der Lizenzgebühr durch die Lizenznehmer:in ist unter Ziffer III.D.4. beschrieben.

## IV. Rechte der Nutzer:innen

### A. Umfang der Nutzung der Lösung

1. Die Lösung findet nur für die Berichterstattung und das Fallmanagement von Hinweisgeber-Berichten über interne Hinweisgeber-Lösungen Anwendung. Eine interne Hinweisgeber-Lösung bedeutet, dass nur Mitarbeitende oder Betriebs- bzw. Organisationsangehörige jeder Art, Dienstleistende und sonstige Geschäftspartner:innen, Kund:innen und externe Berater:innen (z.B. Anwälte und Buchhalter:innen) Meldungen in die Lösung einreichen können. Im Falle von öffentlichen Stellen kommen alle weiteren Personengruppen hinzu, die einen die in einem faktischen Zusammenhang mit dem Tätigkeitsbereich oder den Aufgaben der öffentlichen Stelle stehen.
2. Die Nutzer:in ist nur berechtigt, die Lösung im bestimmungsgemäßen Sinne zu verwenden, also als Meldeplattform für Hinweisgeber-Fälle. Die Nutzer:in verwendet die Lösung nicht für kriminelle Zwecke (z.B. Drogenhandel, Verbreitung von Pornografie usw.) oder auf andere Weise gegen anwendbare gesetzliche Regelungen verstößt.
3. HintHub ist berechtigt zu untersuchen, ob die Beschränkungen der Verwendung der Lösung respektiert werden. HintHub ist berechtigt, bei Nichteinhaltung mit den Bestimmungen in diesem Abschnitt den Service fristlos zu kündigen (vgl. Ziffer III.G.2.).

### B. Lizenz

1. Der Nutzungsvertrag gewährt der Nutzer:in eine zunächst zeitlich nicht begrenzte, nicht-exklusive Lizenz zur Nutzung der Lösung, der jederzeit mit einer Frist von 6 Monaten eine zeitliche Begrenzung zugeteilt werden kann. Die Lizenz unterliegt den Einschränkungen und Bedingungen, die in diesem Vertrag festgelegt sind.
2. Die Rechte an geistigem Eigentum sind Eigentum von HintHub und werden in keiner Weise an die Nutzer:in, die Kund:in oder die Lizenznehmer:in übertragen.
3. HintHub entschädigt die Lizenznehmer:in nicht für jegliche Inanspruchnahme Dritter im Falle einer Verletzung deren Rechte.

### C. Daten

1. Alle Daten der Nutzer:innen, Kund:innen und Lizenznehmer:innen, die in der Lösung enthalten sind, sind Eigentum ebendieser.
2. Die Daten der in der Lösung gemeldeten Vorfälle werden verschlüsselt. Nur der Lizenznehmer (nicht HintHub) hat Zugriff auf den privaten Schlüssel, der zur Entschlüsselung der Daten erforderlich ist.
3. Die Nutzer:in ist dafür verantwortlich, die editierbaren Texte in der Lösung zu pflegen und auf dem neuesten Stand zu halten.
4. Sowohl die Nutzer:innen, Kund:innen und Lizenznehmer:innen als auch HintHub sind berechtigt, Rechte und Pflichten gemäß diesen Bedingungen an Dritte zu übertragen, wenn der Grund eine Änderung der Eigentumsstruktur ist.
5. HintHub ist berechtigt, den Nutzer:innen, den Kund:innen, den Lizenznehmer:innen E-Mail-Benachrichtigungen über die Lösung zu senden, z.B. Informationen zum Status, Benachrichtigungen, Newsletter und Informationen über neue Funktionalitäten.

### D. Lizenzgebühren, Zahlungen und Rechnungen

1. Die Lizenznehmer:in zahlt HintHub eine Lizenzgebühr für die Nutzung der Lösung. Die Preise sowie eine aktuelle Leistungsbeschreibung sind auf hinthub.eu/plans zu finden.
2. Zahlungen erfolgen online mit einer gängigen Kreditkarte, SEPA-Lastschrifteinzug, Diensten wie PayPal, o.ä.
3. Zahlungen können auch über einen Zahlungsdienstleister abgewickelt werden. Wenn dies der Fall ist, ist die Datenweitergabe zu diesem und von diesem in der Datenschutzerklärung zu finden, die unter hinthub.eu/privacy einsehbar ist.
4. Die Lizenznehmer:in akzeptiert, dass die Zahlung als wiederkehrende Zahlung mit dem Zahlungsanbieter [CHARBEE INC.](https://www.chargebee.com/) erstellt wird.
5. Die Zahlung erfolgt in Euro.
6. HintHub speichert Rechnungen und Zahlungsinformationen die selbst oder durch einen Zahlungsanbieter verarbeitet werden.
7. HintHub ist berechtigt, die Preise nach eigenem Ermessen, jedoch immer nach Maßgabe der markt- und geschäftsüblichen Modalitäten, zu ändern. Eine Preisänderung ist der Lizenznehmer:in mitzuteilen. Preisänderungen werden ab der ersten Lizenzperiode nach der Benachrichtigung wirksam.
8. HintHub ist jederzeit berechtigt, die entgeltfreie Variante der Lösung entgeltpflichtig zu machen. Eine Preisänderung bzw. Preiseinführung dieser Art wird 14 Tage nach Mitteilung an die Nutzer:innen wirksam. Zur in Kenntnis Setzung genügt eine elektronische Information wie z.B. eine E-Mail. Die Einführung einer Gebühr für zuvor kostenfreie Varianten eröffnet ein Sonderkündigungsrecht von ebenfalls 14 Tagen.
9. Wenn die Lizenznehmer:in die wiederkehrende Lizenzgebühr nicht bezahlt, wird der Zugang nach einer schriftlichen Mahnung 30 Tage nach Beginn der neuen Lizenzperiode gesperrt. Hat die Lizenznehmer:in die Lizenzgebühr innerhalb von weiteren 7 Tagen noch nicht bezahlt, wird eine zweite Erinnerungs-E-Mail verschickt. Hat die Lizenznehmer:in die Lizenzgebühr 44 Tage nach Beginn einer Lizenzperiode nicht bezahlt, ist HintHub berechtigt, den Service zu kündigen und die in der Lösung gespeicherten Daten zu löschen.
10. Die Lizenzgebühren werden zzgl. der jeweils geltenden gesetzlichen MwSt. in Rechnung gestellt. 

### E. Paketänderung

1. Es ist nicht möglich, das aktuelle Paket auf ein anderes Paket mit einer niedrigeren Lizenzgebühr herabzusetzen. Ausnahme hiervon sind etwaige Testperioden, die direkt nach der Registrierung angeboten werden. Im Rahmen dieser, können Kund:innen für einen begrenzten Zeitraum entgeltfrei den Funktionsumfang eines generell kostenpflichtigen Pakets nutzen. Zum Ende der Periode müssen sich die Kund:innen für die Fortführung eines höherwertigen Pakets aktiv entscheiden oder sich im Funktionsumfang auf die kostenfreie Variante zurückstufen lassen.
2. Die Lizenznehmer:in kann das Paket jederzeit auf ein anderes Paket mit einer höheren Lizenzgebühr im Abschnitt „Unternehmenseinstellungen“ im Hinthub-Portal aktualisieren.
3. Wenn die Lizenznehmer:in ein Paket aufwertet, werden die Vorteile einer Paketänderung sofort wirksam.

### F. Support

1. Jede relevante Seite innerhalb der Lösung enthält an den entsprechenden Stellen zusätzliche Informationen zur Verwendung der jeweiligen Funktionalitäten.
2. Darüber hinaus gibt es einen kostenlosen Support der über E-Mail unter der Adresse, die auf der Startseite der Website im Fußbereich der Seite angegeben ist, angefordert werden kann. Die Reaktionszeiten unterscheiden sich jeweils nach Produktvariante und können in der Leistungsbeschreibung eingesehen werden, erreichbar unter hinthub.eu/plans. Ein telefonischer Support existiert nicht. 

### G. Verwendung und Betrieb

1. Unmittelbar nach Anmeldung der Nutzer:in und/oder der Zahlung der ersten Lizenzgebühr erhält die Nutzer:in, die Kund:in oder die Lizenznehmer:in, eine Initialisierungs-E-Mail, um die Lösung zu aktivieren. Die Lösung kann erst verwendet werden, wenn die Lösung mittels Link aktiviert wird.
2. Verlangt eine Aufsichtsbehörde – gemäß zwingender Vorschrift – rechtmäßig Zugang zu den Daten der Nutzer:innen in der Lösung, ist die Nutzer:in verpflichtet, an der Übergabe der Daten an die Behörde nach besten Kräften mitzuwirken. HintHub kann durch die bestehende Ende-zu-Ende-Verschlüsselung keinen Zugang zur Kommunikation zwischen Nutzer:innen, Kund:innen, Lizenznehmer:innen und Hinweisgeber:innen ermöglichen. 

### H. Verfügbarkeit der Lösung

1. Die Lösung wird jedes Jahr mit etwa 8 bis 10 Versionsupdates aktualisiert und verbessert. Versionsupdates können jederzeit durchgeführt werden. Die Ausfallzeit jedes größeren Updates hängt vom Inhalt des Versionsupdates ab. In der Regel beträgt die Ausfallzeit nicht mehr als 60 Minuten.
2. Jedes Jahr wird eine Reihe von kleineren Updates/Patches veröffentlicht. Die Ausfallzeiten dieser einzelnen kleineren Updates/Patches sind für den Lizenznehmer nicht bemerkbar. Die Implementierung von kleineren Updates/Patches wird nicht im Voraus kommuniziert.
3. HintHub gewährleistet eine Betriebszeit. Der Verfügbarkeitsprozentsatz ist in der Leistungsbescheibung unter hinthub.eu/plans einsehbar. Dieser wird pro Kalenderquartal berechnet. Nutzer:innen und Kund:innen, die die Lösung entgeltfrei nutzen, haben keinen Anspruch auf eine Verfügbarkeit der Lösung. 

### I. Weiterentwicklung und Behebung von Fehlern

1. HintHub entwickelt die Lösung kontinuierlich weiter und fügt neue Funktionalitäten hinzu. HintHub lässt sich durch einen kontinuierlichen allgemeinen Dialog mit den Nutzer:innen der Lösung zur Entwicklung neuer Funktionalitäten inspirieren. HintHub priorisiert, inwieweit und in welcher Reihenfolge neue Funktionalitäten implementiert werden.
2. Die Behebung wesentlicher Fehler beginnt innerhalb von 24 normalen Arbeitsstunden nachdem der Fehler bei HintHub aufgetreten ist. Die Fehlerbehebung wird ohne unangemessene Verzögerung fortgesetzt, bis der Fehler behoben ist.
3. Die Lizenznehmer:in darf sich nicht auf die Verletzung eines schwerwiegenden Fehlers berufen, während HintHub den Fehler behebt, vorausgesetzt, HintHub behebt den Fehler innerhalb von 96 Stunden ab dem Zeitpunkt der Mitteilung des Fehlers an HintHub.
4. Unwesentliche Fehler werden in einem kleinen Update/Patch so schnell wie möglich behoben, wobei zu berücksichtigen ist, inwieweit sich der Fehler auf die Lösung auswirkt. Die Lizenznehmer:in darf sich nicht auf eine Verletzung der Leistung bei geringfügigen Fehlern berufen.
5. Die Unterscheidung zwischen schwerwiegenden und unbedeutenden Fehlern wird ausschließlich von HintHub vorgenommen. Die folgenden Fehler werden jedoch immer als schwerwiegende Fehler angesehen:
5.1. Fehler, die dazu führen, dass die gesamte Lösung nicht verfügbar ist;
5.2. Fehler, die dazu führen, dass Berichte nicht im Berichtsportal der Lösung übermittelt werden können;
5.3. Fehler, die dazu führen, dass sich eine Nutzer:in einer Lizenznehmer:in nicht an der Lösung anmelden kann und
5.4. Fehler, die dazu führen, dass es für Nutzer:innen von Lizenznehmer:innen und Hinweisgebenden nicht möglich ist, über die Lösung zu kommunizieren.
6. HintHub ist nicht verantwortlich für Fehler in der Lösung, die durch eine falsche Verwendung oder einen Missbrauch der Lösung durch Nutzer:innen oder Dritte, verursacht werden.
7. Die Nutzer:innen sind für die korrekte Konfiguration der Lösung zur Nutzung für ihre jeweiligen Unternehmen verantwortlich.

### J. Sicherheit

1. HintHub gewährleistet, dass die Sicherheit der Lösung dem Stand der Technik im Rahmen von Hosting von internetbasierten Anwendungen entspricht. Die Datenverarbeitungsbedingungen von HintHub enthalten eine Liste der technischen und organisatorischen Maßnahmen, die ergriffen wurden, um ein hohes Maß an Informationssicherheit zu gewährleisten.
2. HintHub haftet nicht für die Folgen feindlicher Angriffe auf die Lösung.
3. Nutzer:innen, Kund:innen und Lizenznehmer:innen sind verpflichtet, die allgemein anerkannten Sicherheitsrichtlinien einzuhalten, einschließlich (aber nicht beschränkt auf):
3.1. Keine Weitergabe von Passwörtern der Lösung - auch nicht innerhalb der selben Organisation.
3.2. Anmeldung an der Lösung nur über sichere Computer mit installierter aktueller Antivirus-/Antimalware-Software.
3.3. Regelmäßige Überprüfung der Accounts der Administratoren und Sperre bzw. Aktualisierung der Accounts der Administratoren im Falle der Trennung von einer Mitarbeiter:in oder des Wechsels der Position einer Mitarbeiter:in.
3.4. Im Bereich „Berichte“ kann die Lizenznehmer:in Listen der in ihrer HintHub-Anwendung erfolgten Aktivitäten herunterladen. Dies sollte regelmäßig erfolgen, um Unregelmäßigkeiten frühzeitig festzustellen.
4. Täglich erfolgt ein Backup der Lösung. Dieses Backup wird für 30 Tage gespeichert und danach automatisch gelöscht. Am Ende jeden Monats wird ein monatliches Backup erstellt. Dieses monatliche Backup wird für drei Jahre aufbewahrt und danach gelöscht.
5. Falls die Lizenznehmer:in von HintHub ein Backup ihrer Daten wünscht, muss diese Anfrage schriftlich an HintHub gestellt werden. Die Kosten hierfür sind gesondert zu vergüten und betragen 500,00 Euro zzgl. der jeweiligen gesetzlichen MwSt. für die gesonderte Sicherung der Daten, soweit die Veranlassung der Datensicherung nicht auf HintHub zurückzuführen ist.

### K. Verarbeitung personenbezogener Daten

1. Die Verarbeitung personenbezogener Daten wird durch die Datenverarbeitungsbedingungen von HintHub geregelt.
2. Die Lösung ist so konfiguriert, dass es den Anforderungen der Gesetzgebung in der EU zur Verarbeitung personenbezogener Daten entspricht (DSGVO). Passwörter werden zwingend vorausgesetzt. Die Mindestanforderungen daran, sind in der Lösung angelegt, werden den Nutzer:innen bereits bei der ersten Registrierung angezeigt und sind für die Nutzung verpflichtend.
3. Die Nutzer:innen, Kund:innen und Lizenznehmer:innen sind verpflichtet, die Geheimhaltung von Passwörtern und Benutzernamen sicherzustellen.

### V. Schlussbestimmungen

1. Die Bedingungen gelten automatisch ab dem Zeitpunkt, zu dem sich eine Nutzer:in am System erstmalig anmeldet bzw. eine Nutzer:in für eine Lizenznehmer:in die Bestellung des Service aufgibt.
2. Änderungen und Ergänzungen dieses Vertrags bedürfen zu ihrer Wirksamkeit der Schriftform und müssen durch alle Parteien akzeptiert worden sein. Dies gilt auch für die Änderung dieser Bestimmung. § 305b BGB bleibt hiervon unberührt.
3. Die Bedingungen können für Nutzer:innen, Kund:innen und Lizenznemer:innen können jederzeit mit einer Frist von 30 Tagen geändert werden.
4. Wesentliche Änderungen der Bedingungen werden den Nutzer:innen, Kund:innen und Lizenznehmer:innen per E-Mail an die vom im Abschnitt „Leistungen“ im Administratorportal hinterlegte E-Mail-Adresse mitgeteilt.
5. Die aktualisierten Bedingungen können jederzeit hier heruntergeladen werden: hinthub.eu/terms. Die Datenverarbeitungsbedingungen von HintHub können unter hinthub.eu/privacy heruntergeladen werden.
6. Ausschließlicher Gerichtsstand für alle Streitigkeiten aus oder im Zusammenhang mit diesem Vertrag ist Wiesbaden.
7. Sollten einzelne Bestimmungen dieses Vertrags ganz oder teilweise auslegungs- oder ergänzungsbedürftig sein, so hat die Auslegung oder Ergänzung in der Weise zu erfolgen, dass sie dem Geist, Inhalt und Zweck dieses Vertrags bestmöglich gerecht wird. Es sollen dabei jene Regelungen gelten, die die Parteien vernünftigerweise vereinbart hätten, wenn sie beim Abschluss dieses Vertrags die Auslegungs- oder Ergänzungsbedürftigkeit der betreffenden Regelung bedacht hätten. Sollte dieser Vertrag eine Regelungslücke aufweisen, so gilt die vorstehende Ziffer V.7. entsprechend.
8. Jede Partei trägt ihre Kosten selbst, soweit dieser Vertrag nichts anderes bestimmt.
9. Dieser Vertrag und seine Auslegung unterliegen ausschließlich deutschem Recht.
