
# Terms and Conditions

As at: 28 June 2022

## I. Scope of Law

HintHub GmbH, Hauptstr. 28, 15806 Zossen, Germany (hereinafter "HintHub") operates a whistleblower software-as-a-service platform called HintHub that enables business customers and public institutions to comply with, inter alia, the Whistleblower Directive (EU) 2019/1937.
This document, hereinafter also referred to as the "Terms and Conditions" or „T&C", governs the contractual terms and conditions for the use of the HintHub Whistleblower Software Platform for all Users, regardless of whether a free or paid version of the Software is used.
3. it is a "Software as a Service" (SaaS) that is operated via a web-based interface and enables legal entities and their employees to communicate with other employees or business partners via the solution without using any software other than an internet browser.
These T&C apply exclusively. Deviating, conflicting or supplementary terms and conditions of the Customer shall only become part of the contract if HintHub has expressly consented to their validity in text form. This requirement of consent shall also apply if HintHub commences performance without reservation in the knowledge of the terms and conditions of another party.

## II. Definitions

1. "Users" means all natural persons who are employees or otherwise authorised persons of private companies, legal entities or public bodies and who use the HintHub Whistleblower Software Platform.
2. "Customer" means all legal entities under private or public law that are contracting parties with HintHub for the use of the HintHub Whistleblower Software Platform.
3. "Licensee" means the subset of Customers who use a version of the Software that is subject to payment, i.e. who pay license fees.
4. "Conditions" means the terms and conditions contained in this document.
5. "Settings" means the section in the HintHub portal where Users can enter information about the Client, in particular their current number of employees and their contact details.
6. "Update" means improvements and functional enhancements and updates are installed as software updates.
7. "User backend" means the area of the solution where users can view and edit notices and, where appropriate, customise the solution to meet the specific needs of their organisations. Whether customisability is possible depends on whether it is a free use or a paid subscription.
8. "Licence Fee" means the fee paid by the Licensee for the Solution Use Right during a Licence Period.
9. "Licence Period" means the period of time for which the Licensee receives the right of use against payment of the Licence Fee.
10. "Licence Types" means the totality of variants of the software, i.e. one free of charge and one or more payable set(s) of functions, to which users, customers and licensees may establish a right of use.
11. "HintHub Portal" or "Solution" means the HintHub application developed by Hinthub GmbH to which Licensees are granted Solution Use Rights under these Terms.
12. "Right of Use" means the Licensee's right to use the Solution in accordance with the Terms.
13. "User Agreement" means the agreement between HintHub and Licensee for the use of HintHub.
14. "Party" or "Parties" means the parties to the User Agreement, i.e. Licensee and HintHub, who are subject to these Terms and Conditions and the rights and obligations described below.
15. "Package" or "Variant" means a collection of features of the Solution. The packages vary in their scope of services.
16. "Trial Period" means a period of time commencing upon initial registration with the HintHub Portal and limited in duration. 
17. "Company Settings", "Company Profile" or "Company Profile" means the section in the Solution where Licensees can view their currently valid Package, change the Package, book Additional Services, download invoices and maintain contact and billing information, etc..
18. "Services" means the services provided by HintHub to Licensee through the provision of Solution Usage Rights.
19. "Service description" defines the scope of individual sub-aspects of the software, which differ depending on the package selected. An up-to-date overview of the scope of the respective packages and the respective associated prices can be found at [hinthub.eu/plans](https://www.hinthub.eu/plans).
20. "Termination" means the date on which a Party's written notice of termination becomes effective, subject to the termination rights and any associated time limits set out in this document.
21. "Additional Services" means the services provided by HintHub to Licensees in addition to the Packages in return for payment with the Booking.
22. "Confidential Information" means any information that HintHub or Customer protect against unrestricted disclosure to third parties or which, under the circumstances of disclosure or according to its content, is to be considered confidential. In any event the following information shall be deemed to be Confidential Information of HintHub: all software, programs, tools, prices, data or other materials provided by HintHub to Users, Customers or Licensees in advance of or pursuant to the Contract.
23. "End-to-end encryption“ or "E2E" is a technology built into the Solution that encrypts communications between Users, Customers, Licensees and Notifiers in such a way that only the sender and recipient of a message can decrypt it and thus read it in plain text. This technology does not allow HintHub to read or decrypt the contents of the communication at any time.

## III. General Conditions

### A. Confidentiality

1. The parties undertake to protect all confidential information of the other party obtained before and in the course of the performance of the contract for an unlimited period of time in the same way as they protect their own comparable confidential information, but at least to treat it confidentially with reasonable care.
2. HintHub may use data collected during the Term that falls under Confidential Information in aggregated, anonymised form, provided that such data is collected from more than one User, Customer or Licensee and does not identify the employees of the respective companies or institutions.
3. the foregoing clauses shall not apply to confidential information which 
3.1. have been independently developed by the recipient without recourse to the disclosing party's Confidential Information;
3.2. has become generally available to the public without breach of contract by the receiving party or has been lawfully obtained without a duty of confidentiality from a third party authorised to provide such Confidential Information;
3.3. was known to the Recipient without restriction at the time of disclosure;
3.4. is exempt from the foregoing upon the written consent of the disclosing party; or
3.5. required by a competent court or authority and a mandatory regulation.
HintHub is entitled to publish, e.g. in a reference list, as a representation or picture or logo on the HintHub website or in newsletters, that users and their organisations belong to the customer portfolio of HintHub.

### B. Licence types, durations

1. the package is selected by a user authorised by the licensee for the licensee when he or she signs up for the solution and can be changed for the future during the subscription. The selection of a higher-value package with a greater scope of services is possible at any time, a downgrade only at the end of the licence period. Excluded from this are any test periods that are offered directly after registration. Within the scope of these, customers can use the functional scope of a generally chargeable package free of charge for a limited period of time. At the end of the period, customers must actively opt for the continuation of a higher-value package or have the range of functions downgraded to the free variant. 2.
The first licence period runs for 12 months. At the end of each licence period a new licence period shall automatically commence, unless this agreement has been terminated in accordance with Section III.F.. The second and each subsequent Licence Period shall also run for 12 months, unless Licensee:in is offered a discounted period with a different term by HintHub. The first Licence Period shall commence on the date on which Licensee has paid the first Licence Fee in the Hinthub Portal.

### C. Test periods

1. the length of the term of test periods is displayed to the user during the first registration. During this period, the user has free access to functions that are partially or completely assigned to a higher-value package. At the end of the period, the user must decide whether he or she wishes to retain the range of functions for a fee or switch to a lower-value package. A detailed description of the scope of services is defined in the service description, which can be found at [hinthub.eu/plans](https://www.hinthub.eu/plans).
2. there is no entitlement to this period as such and to a specific scope of functions within the term of the test period. HintHub is entitled at any time to partially or completely withdraw the functions made available during this period. If the user does not make a decision for a certain package at the end of the period, she will be downgraded to the free variant. If the user chooses a chargeable package, the first licence period shall commence upon payment.

### D. Information about users, customers and licensees; notifications

1. users keep the information about the company or authority, number of employees and contact information up-to-date in the solution for clients or licensees. Users acting on behalf of licensees also do this with payment and billing information.
2. all notifications, communications, invoices, etc. will be sent by HintHub to the Users, Clients or Licensees at the email address stored in the Portal in the "Settings" section.

### E. General warranty

1. HintHub makes no representations or warranties with respect to the provision of the Services.
2. in the event of defective performance by HintHub, the Licensee shall give HintHub the opportunity to remedy the defect at least twice within a reasonable period of time, unless this is unreasonable in the individual case or special circumstances exist which, after weighing the interests of both parties, justify termination of the order by the Licensee. HintHub may, at its own discretion, remedy the defect or provide the service again free of defects. If the subsequent performance fails, Licensee:in has the right to reduce the remuneration or to withdraw from the contract under the statutory conditions. Claims for damages shall only exist in accordance with these contractual conditions. However, there shall be no claims for damages if the deviation from the quality owed is only insignificant.

### F. Liability

1. in all cases of contractual and non-contractual liability, HintHub shall only pay damages or reimburse futile expenses to the extent determined below:
1.1 In the event of a breach of material contractual obligations caused by slight negligence, HintHub's liability shall be limited to the amount of the foreseeable damage typical for the contract, except in the cases of clause E.1 or E.2. Material contractual obligations are, in the abstract, obligations the fulfilment of which makes the proper performance of a contract possible in the first place and compliance with which the contracting parties may regularly rely on. Any further liability of HintHub is excluded. Liability is limited for each individual case of damage and for all cases of damage together arising from or in connection with the contractual relationship to five times the total amount of the Licensee's remuneration for the respective Service in the respective calendar year.
1.2 HintHub is not liable for conditions which are beyond the control of HintHub and which were not foreseeable, such as general strikes, war, earthquakes, natural disasters, strikes, lockouts and the like.
1.3 A limitation period of one year applies to all claims against HintHub for damages or reimbursement of futile expenses in the case of contractual and non-contractual liability. The limitation period shall commence at the time determined by law (in Germany: §199 para. 1 BGB). It occurs at the latest with the expiry of five years from the accrual of the claim. This does not apply (i) in the case of liability for intent or gross negligence, (ii) in the case of personal injury, or (iii) under the Product Liability Act. The deviating limitation period for claims due to material defects and defects of title shall remain unaffected by the provisions of this paragraph.

### G. Termination

1. the Service is made available to the User, the Customer and the Licensee until termination of the contract by HintHub in accordance with clause IV.H. HintHub is entitled to terminate the Service with 12 months' notice to the end of a calendar month. HintHub is entitled to terminate the Service with 12 months' notice to the end of a calendar month. 
2. in addition, HintHub is entitled to terminate the Service immediately and without further notice at any time if Licensee's use of the Solution violates the provisions of Section IV.A. of these Terms. 
3. the Licensee may terminate the Service at any time by logging out in the "Company Settings" or "Company Profile" section of the HintHub Portal. Termination will take effect at the end of the last day of the current Licence Period.
4. if the Licensee has not paid the Licence Fee 30 days after the start of a Licence Period, HintHub is entitled to pause the Service until payment is received, during which time notices may be received from outside sources, but the Licensee will not have access to such receipts. If the Licensee fails to pay the Licence Fee within the time limit even after a reminder, a further period of 14 days will commence during which the Licensee will be given the opportunity to settle the overdue fees. After expiry of this period and further default, HintHub is entitled to terminate the Service and delete all data on the HintHub portal (cf. Section IV.D.9).
5. any prepaid licence fee shall not be refunded to the Licensee.
6. HintHub is entitled to delete the Licensee's Solution and the data stored in the Solution after the provision of the service within the framework of the GDPR.
7. it is possible for the user to export the notices of whistleblowers that have arrived in the solution as well as a listing of the changes to these notices. If the user, client or licensee wishes to export data from the solution in the event of imminent termination of the contract, the licensee must perform the data export no later than the last day of the licence period. If the Licensee does not export the data in time, the data will be deleted after six months at the latest and the export of the data is no longer possible.

### H. Breach of duty

1. either party may rely on a breach if the other party fails to perform its obligations under these Conditions. However, before being entitled to invoke a breach, the party invoking the breach shall give the defaulting party a period of seven Business Days to remedy the breach. The notice shall describe which obligations are considered to have been breached.
2. if the breach is material, the party relying on it may terminate this contract. In this case, the notice must state that the relying party intends to terminate the Service. 3.
3. a breach for non-payment of the licence fee by the licensee is described in clause III.D.4.

## IV. Rights of the users

### A. Scope of use of the solution

1. the solution applies only to the reporting and case management of whistleblower reports through internal whistleblower solutions. An internal whistleblowing solution means that only employees or members of the organisation of any kind, service providers and other business partners, customers and external consultants (e.g. lawyers and accountants) can submit reports to the solution. In the case of public bodies, all other groups of persons who have a factual connection with the field of activity or the tasks of the public body are added.
2. the user is only entitled to use the solution in the intended sense, i.e. as a reporting platform for whistleblower cases. The User shall not use the Solution for criminal purposes (e.g. drug trafficking, distribution of pornography, etc.) or in any other way that violates applicable legal regulations.
3. HintHub is entitled to investigate whether the restrictions on the use of the Solution are respected. HintHub is entitled to terminate the Service without notice in the event of non-compliance with the provisions in this section (cf. Clause III.G.2.).

### B. Licence

1. the licence agreement grants the user an initial non-exclusive licence to use the solution for an indefinite period of time, which may be extended at any time with 6 months' notice. The licence is subject to the limitations and conditions set out in this Agreement.
2. the intellectual property rights are owned by HintHub and are not transferred in any way to the User, the Customer or the Licensee.
3. HintHub will not indemnify the Licensee for any claims by third parties in the event of an infringement of their rights.

### C. Data

1. all data of users, clients and licensees contained in the solution are the property of the same.
2. the data of the incidents reported in the Solution will be encrypted. Only the Licensee (not HintHub) will have access to the private key required to decrypt the data.
3. the user is responsible for maintaining and updating the editable texts in the solution.
4. both the users, customers and licensees and HintHub are entitled to transfer rights and obligations under these terms and conditions to third parties if the reason is a change in the ownership structure.
5. HintHub is entitled to send the Users, Customers, Licensees email notifications about the Solution, e.g. status information, notifications, newsletters and information about new functionalities.

### D. Royalties, payments and invoices

1. the licensee pays HintHub a licence fee for the use of the solution. The prices as well as a current service description can be found on hinthub.eu/plans.
2. payments are made online with a major credit card, SEPA direct debit, services such as PayPal, or similar.
3. payments can also be made via a payment service provider. If this is the case, the data transfer to and from this payment service provider can be found in the privacy policy, which can be viewed at [hinthub.eu/privacy](https://www.hinthub.eu/privacy).
4. the licensee accepts that the payment will be created as a recurring payment with the payment provider [CHARBEE INC.](https://www.chargebee.com/).
5. the payment will be made in Euro.
6. HintHub stores invoices and payment information processed by itself or by a payment provider.
7. HintHub is entitled to change prices at its sole discretion, but always in accordance with market and business practice. The Licensee must be notified of any price change. Price changes will take effect from the first Licence Period following notification.
8. HintHub is entitled at any time to charge for the free version of the Solution. A price change or price introduction of this kind becomes effective 14 days after notification to the Users. Electronic information such as an e-mail is sufficient for notification. The introduction of a fee for previously free variants shall also open up a special right of termination of 14 days.
9. if the licensee does not pay the recurring licence fee, access will be blocked after a written reminder 30 days after the start of the new licence period. If the licensee has not paid the licence fee within a further 7 days, a second reminder email will be sent. If the Licensee has not paid the License Fee 44 days after the start of a License Period, HintHub is entitled to terminate the Service and delete the data stored in the Solution. 10.
10. The licence fees are invoiced plus the applicable statutory VAT. 

### E. Change of the package

1. it is not possible to downgrade the current package to another package with a lower licence fee. Exceptions to this are any test periods that are offered directly after registration. Within the scope of these, customers can use the range of functions of a generally chargeable package free of charge for a limited period of time. At the end of the period, the customer must actively decide to continue with a higher-value package or to have the range of functions downgraded to the free variant.
2. the licensee can upgrade the package at any time to another package with a higher licence fee in the "Company settings" section in the Hinthub portal.
3. if the licensee upgrades a package, the benefits of a package change will take effect immediately.

### F. Support

1. each relevant page within the solution contains additional information on the use of the respective functionalities at the appropriate places.
2. in addition, there is a free support service which can be requested by e-mail at the address given on the start page of the website in the footer area. The response times differ according to the product variant and can be found in the service description, which can be accessed at [hinthub.eu/plans](https://www.hinthub.eu/plans).

### G. Use and operation

1. immediately after registration of the user and/or payment of the first licence fee, the user, the customer or the licensee receives an initialisation e-mail to activate the solution. The Solution cannot be used until the Solution is activated by means of a link.
2. if a supervisory authority - in accordance with mandatory regulations - lawfully requests access to the user's data in the solution, the user is obliged to cooperate to the best of her ability in handing over the data to the authority. HintHub cannot provide access to the communication between users, clients, licensees and whistleblowers due to the existing end-to-end encryption.

### H. Availability of the solution

1. the solution is updated and improved every year with approximately 8 to 10 version updates. Version updates can be carried out at any time. The downtime of each major update depends on the content of the version update. Generally, the downtime is no more than 60 minutes.
2. a number of minor updates/patches are released each year. The downtime of these individual minor updates/patches is not noticeable to the Licensee. The implementation of minor updates/patches will not be communicated in advance.
3. HintHub guarantees an uptime. The availability percentage can be viewed in the service description at hinthub.eu/plans. This is calculated per calendar quarter. Users and customers who use the solution free of charge are not entitled to an availability of the solution.

### I. Further development and correction of errors

1. HintHub continuously develops the solution and adds new functionalities. HintHub is inspired to develop new functionalities through a continuous general dialogue with the users of the solution. HintHub prioritises to what extent and in what order new functionalities are implemented.
2. significant bug fixing will start within 24 normal working hours after the bug has been encountered by HintHub. Bug fixing will continue without unreasonable delay until the bug is fixed.
3. the Licensee may not rely on a breach of a Material Error while HintHub is fixing the Error, provided that HintHub fixes the Error within 96 hours from the time HintHub is notified of the Error.
4. insignificant errors will be fixed in a minor update/patch as soon as possible, taking into account the extent to which the error affects the solution. The Licensee may not invoke a breach of performance for minor errors.
5. the distinction between serious and insignificant errors is made exclusively by HintHub. However, the following errors are always considered serious errors:
5.1. errors that cause the entire solution to be unavailable;
5.2. errors that result in reports not being able to be submitted in the solution's reporting portal;
5.3. errors that result in a Licensee User being unable to log in to the Solution; and
5.4. errors that result in a Licensee User and a Whistleblower not being able to communicate via the Solution.
6. HintHub is not responsible for errors in the Solution caused by misuse or abuse of the Solution by Users or third parties.
7. users are responsible for the correct configuration of the solution for use by their respective companies.

### J. Security

1. HintHub ensures that the security of the Solution is state of the art in the context of hosting Internet-based applications. HintHub's data processing terms and conditions contain a list of technical and organisational measures taken to ensure a high level of information security.
2. HintHub is not liable for the consequences of hostile attacks on the Solution.
3. users, customers and licensees are required to comply with generally accepted security guidelines, including (but not limited to):
3.1. not to share passwords of the Solution - even within the same organisation.
3.2. logging into the Solution only via secure computers with up-to-date anti-virus/anti-malware software installed.
3.3. regularly reviewing administrators' accounts and blocking or updating administrators' accounts in case of separation from a staff member or change of position of a staff member.
3.4 in the "Reports" section, the licensee can download lists of activities that have taken place in her HintHub application. This should be done regularly in order to detect irregularities at an early stage. 4.
4. a daily backup of the solution is made. This backup is stored for 30 days and then automatically deleted. A monthly backup is made at the end of each month. This monthly backup is kept for three years and then deleted.
5. if the Licensee wishes HintHub to back up its data, this request must be made in writing to HintHub. The costs for this are to be paid separately and amount to 500.00 Euro plus the respective statutory VAT for the separate backup of the data, insofar as the initiation of the data backup is not attributable to HintHub.

### K. Processing of personal data

1. the processing of personal data is governed by HintHub's data processing terms and conditions.
2. the solution is configured to comply with the requirements of the legislation in the EU on the processing of personal data (GDPR). Passwords are mandatory. The minimum requirements for passwords are created in the solution, are displayed to users when they register for the first time and are mandatory for use.
3. Users, customers and licensees are obliged to ensure the confidentiality of passwords and user names.

### V. Final Provisions

1. these terms and conditions shall apply automatically from the time a user registers with the system for the first time or a user places an order for the service on behalf of a licensee.
2. amendments and additions to this Agreement must be in writing and must be accepted by all parties in order to be effective. This also applies to the amendment of this provision. § 305b BGB remains unaffected.
3. the terms and conditions for users, customers and licensees can be changed at any time with a notice period of 30 days. 4.
4. significant changes to the terms and conditions will be communicated to users, customers and licensees by e-mail to the e-mail address provided by the user in the "Services" section of the administrator portal. 5.
5. the updated terms and conditions can be downloaded at any time here: [hinthub.eu/terms](https://www.hinthub.eu/terms). HintHub's data processing terms and conditions can be downloaded at [hinthub.eu/privacy](https://www.hinthub.eu/privacy).
6. the exclusive place of jurisdiction for all disputes arising from or in connection with this contract is Wiesbaden.
7. should individual provisions of this Agreement require interpretation or supplementation in whole or in part, such interpretation or supplementation shall be made in such a way as to best reflect the spirit, content and purpose of this Agreement. The provisions shall apply which the parties would have reasonably agreed if they had considered the need for interpretation or supplementation of the relevant provision when concluding this contract. If there is a loophole in this contract, clause V.7. above shall apply accordingly. 8.
8. each party shall bear its own costs unless otherwise provided for in this contract.
9. this contract and its interpretation shall be governed exclusively by German law.
