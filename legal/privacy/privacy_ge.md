# Willkommen bei HintHub!
## 1. Ihre Privatsphäre ist uns wichtig
### 1.1 Wer sind wir?
Die HintHub GmbH (später “HintHub” oder „wir" genannt) stellt eine Whistleblower Software-as-a-Service Plattform mit gleichem Namen zur Verfügung, die es Nutzer:innen ermöglicht, einen Meldekanal zu etablieren, mit dem Hinweisgeber:innen Informationen mit den Nutzer:innen bzw. den zugehörigen Unternehmen oder Organisationen teilen können, deren Inhalte potenziell oder faktisch auf Verstöße von internen oder externen Regelungen, Gesetzen, Codices oder anderweitigen Normen dieser jeweiligen Unternehmen oder Organisationen hindeuten.
Wenn Sie direkt mit uns sprechen möchten, finden Sie unsere Kontaktdaten am Ende dieser Datenschutzerklärung. Einen internen oder externen Datenschutzbeauftragten haben wir nicht benannt, da wir hierzu qua Unternehmensgröße durch deutsches und europäisches Recht nicht verpflichtet sind.
### 1.2 Wozu dient diese Datenschutzerklärung?
Diese Datenschutzerklärung beschreibt, wie HintHub Ihre persönlichen Daten sammelt, verwendet, speichert, teilt und sichert. Sie gilt bei Zugriff auf, Besuch von oder Verwendung eines beliebigen Teils unserer Website oder unseres Dienstes. Sie können diese Datenschutzerklärung jederzeit über unsere Website hinthub.eu/privacy abrufen und herunterladen, speichern und drucken. Je nachdem, wie sie unsere Seite oder Dienst verwenden, gelten einige Teile dieser Erklärung für Sie, andere nicht – aus jedem Kapitel geht deutlich hervor, ob es für Sie gilt.
Bitte lesen Sie diese Datenschutzerklärung und unsere Allgemeinen Geschäftsbedingungen sorgfältig. Ihre Zustimmung zu diesen Bestimmungen ist zur Nutzung unseres Dienstes erforderlich. Sie können unseren Dienst nicht nutzen, wenn Sie diesen Bestimmungen nicht zustimmen.
### 1.3 Was bedeutet „Personenbezogene Daten"
In dieser Datenschutzerklärung werden wir viel über „personenbezogene Daten" sprechen. Dieser Begriff ist in Artikel 4 der Europäischen Datenschutzgrundverordnung (DSGVO) wie folgt definiert und wird in diesem Sinn für die Zwecke dieser Datenschutzerklärung verwendet:
„Personenbezogene Daten bezeichnet alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person (im Folgenden „betroffene Person") beziehen; als identifizierbare natürliche Person wird eine natürliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann, die Ausdruck der physischen, wirtschaftlichen, kulturellen oder sozialen Identität dieser natürlichen Person sind."
### 1.4 Website-Besucher:innen, Nutzer:innen und Hinweisgeber:innen
HintHub wird von Website-Besucher:innen (Menschen, die unserer Webseite besuchen, inklusive allgemeiner Besucher:innen, die sich nicht mit einem Account bei uns registrieren), Nutzer:innen (Personen, die über unsere Webseite einen Meldekanal für ihr Unternehmen betreiben) und Hinweisgeber:innen (Personen, die bereits näher bezeichnete Hinweise abgeben) verwendet. Wie wir Ihre Daten speichern und handhaben, hängt zum großen Teil davon ab, ob Sie ein:e Website-Besucher:in, ein:e Nutzer:in und/oder ein:e Hinweisgeber:in sind. Aus diesem Grund haben wir diese Datenschutzerklärung in drei Abschnitte aufgeteilt.
## 2. Datenschutz für Website-Besucher:innen
### 2.1 Wer sind Website-Besucher:innen?
Wenn und während Sie unsere Webseite hinthub.eu besuchen (auch ohne Registrierung eines Accounts) sind Sie ein:e „Website-Besucher:in".
### 2.2 Welche Daten erfassen wir von Website-Besucher:innen?
Während Sie unsere Website besuchen, erfassen wir im Hintergrund folgende Daten von all unseren Website-Besuchern:innen:
* Statistische Nutzungsdaten: Wir erfassen und analysieren Daten darüber, wie Sie unsere Webseite gefunden haben, wie Sie browsen, wie lange Sie bleiben und worauf Sie während Ihres Aufenthaltes klicken.
* Geräte- und Browserdaten: Wir sammeln Daten über das für die Nutzung eingesetzte Gerät (z.B. ob Sie einen Laptop oder ein Smartphone verwenden, dessen Hersteller sowie das Betriebssystem, dass Sie benutzen) und die Anwendung (z.B. ob Sie Chrome oder Firefox verwenden), um Zugang zu unserer Webseite zu erhalten. Dies umfasst auch Ihre öffentliche IP-Adresse, mit der wir Rückschlüsse auf Ihren geografischen Standort ziehen können.
* Verweisdaten: Falls Sie auf unsere Webseite durch einen externen Verweis gelangt sind (wie über einen Link von einer anderen Webseite oder einer E-Mail) speichern wir Informationen über die Quelle, die Sie an uns weitergeleitet hat.
* Daten von „Webhooks" oder „Page-Tags": Wir verwenden sogenannte „Tracking-Services" von Drittanbietern oder von uns selbst, die Webhooks und Page-Tags einsetzen (auch bekannt als Web-Beacons oder Web-Bugs), um aggregierte und anonymisierte Daten über Besucher unserer Webseiten zu sammeln. Diese Daten können sowohl Nutzungs- als auch Website-Besucherstatistiken enthalten.
Falls Sie auf bestimmte Art und Weise mit unserer Webseite interagieren, werden wir außerdem die folgenden Daten über Sie erfassen:
* Freiwillige Daten: Wir können zusätzliche personenbezogene Informationen oder Daten von Ihnen erfassen, wenn Sie uns diese in anderen Zusammenhängen, wie Referenzen oder öffentlichen Inhalten, freiwillig übermitteln.
Rechtsgrundlage für die Erhebung der Daten ist Art. 6 Abs. 1 S. 1 lit. f DSGVO, begründet auf dem Interesse Ihnen den Besuch der Website zu ermöglichen und die dauerhafte Funktionsfähigkeit und Sicherheit unserer Systeme zu gewährleisten. Rechtsgrundlage für die Erhebung der freiwilligen Daten ist Art. 6 Abs. 1 S. 1 lit. b DSGVO.
### 2.3 Wie verwenden wir die über Website-Besucher:innen gesammelten Daten?
Wir verwenden die über Website-Besucher:innen gesammelten Daten, um:
* Ihre Erfahrung zu personalisieren: Ihre Daten helfen uns dabei, besser auf Ihre individuellen Bedürfnisse einzugehen.
* unsere Webseite zu verbessern: Wie streben ständig danach, unsere Web-Angebote aufgrund der Daten und der Feedbacks, die wir von Ihnen erhalten, zu verbessern.
* den Kundendienst zu verbessern: Ihre Daten helfen uns dabei, auf Ihre Kundendienstanfragen und Support-Bedürfnisse effektiver zu reagieren sowie neue Funktionen zu evaluieren oder auszuarbeiten. Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. f DSGVO, beruhend auf dem berechtigten Interesse an der Steigerung der Attraktivität unseres Services.
### 2.4 Wann und mit wem teilen wir Ihre Daten?
HintHub erkennt an, dass Sie uns die Sicherung der Geheimhaltung Ihrer Daten anvertraut haben. Ihr Vertrauen ist uns sehr wichtig, so dass wir Ihre Daten nur unter begrenzten Umständen und in Übereinstimmung mit dem geltenden Gesetz offenlegen oder teilen. Insbesondere können wir Ihre Daten offenlegen gegenüber:
* unseren Service Providern. HintHub nimmt externe Service-Provider in Anspruch, die dabei unterstützen, unsere Dienste für Sie bereitzustellen. Diese umfassen insbesondere Kreditkarten- und Zahlungsverarbeiter, Daten-Hosting-Service Provider und Provider von Webanalyse-Tools. Wir geben diesen Providern Zugang zu Ihren Daten, aber nur in dem Umfang, den sie benötigen, um ihre Dienste für uns auszuführen. Außerdem verpflichten wir diese Service Provider vertraglich dazu, Ihre Daten geheim zu halten und sie nur für die Bereitstellung Ihrer Dienste zu verwenden. Wenn unsere Service Provider Ihre Daten außerhalb der Europäischen Union verarbeiten, kann dies bedeuten, dass Ihre Daten in ein Land mit einem niedrigeren Datenschutzstandard als die Europäische Union übermittelt werden. In solchen Fällen stellt HintHub sicher, dass die jeweiligen Service Provider vertraglich oder anderweitig ein gleichwertiges Datenschutzniveau gewährleisten.
Die von uns in Anspruch genommenen Service-Provider sind:
* Sendinblue GmbH, Köpenicker Straße 126, 10179 Berlin, Deutschland. Mehr Informationen finden Sie unter Allgemeine Nutzungsbedingungen der Sendinblue-Dienste - Sendinblue  und Datenschutz-Übersicht: Alles was Sie wissen müssen!
* Chargebee Inc., 340 South Lemon Avenue Suite 1537 Los Angeles, CA 91789 United States.  Mehr Informationen finden Sie unterTerms of Service - Chargebee Inc, Privacy Policy - Chargebee Inc  und EU-GDPR — Chargebee Docs
* Hetzner Online GmbH, Industriestr. 25, 91710 Gunzenhausen, Deutschland. Mehr Informationen finden Sie unter
* Legal Notice - Hetzner Online GmbH  und
* Data Privacy - Hetzner Online GmbH
* Matomo, InnoCraft, 7 Waterloo Quay PO625, 6140 Wellington, New Zealand. Mehr Informationen finden Sie unter Matomo Privacy Policy und GDPR
* Sentry, Functional Software, Inc., 45 Fremont Street, 8th Floor, San Francisco, CA 94105 United States. Terms of Service  und Privacy Policy sowie Data Security, Privacy, and Compliance Overview
* Cloudflare Inc., 101 Townsend St., San Francisco, California 94107, United States. Terms of Use und Datenschutzrichtlinie
* GitLab B.V. [Privacy Policy](https://about.gitlab.com/privacy/)
 
*Sofern erforderlich oder gesetzlich erlaubt: In wenigen Ausnahmefällen können öffentliche Behörden (wie Gerichte, Regierungsbehörden, Staatsanwaltschaften, Kartellrechtsbehörden und andere) uns dazu auffordern, ihnen gegenüber in Ausübung ihrer Pflichten Daten offenzulegen (z.B. um im Hinblick auf illegale Aktivitäten zu ermitteln, diese zu verhindern oder zu handeln). Wir dürfen Ihre Daten offenlegen, falls erforderlich oder gesetzlich erlaubt. Dies dürfen wir auch dann, wenn wir glauben, dass eine Offenlegung zum Schutz unserer Rechte, Ihrer Sicherheit oder der Sicherheit von anderen nötig ist, und/oder um einem Gerichtsverfahren, Gerichtsbeschluss, einer Vorladung oder einem anderen rechtlichen Verfahren, das uns zugestellt wurde, Folge zu leisten.
*Wenn es zu einer Änderung in der Gesellschafter- oder Eigentümerstruktur von HintHub kommt: Wenn sich die Gesellschafterstruktur unseres Unternehmens oder in wesentlichen Bereichen des Unternehmens ändert (entweder durch einen Share Deal oder einen Asset Deal), oder wenn wir eine Umstrukturierung vornehmen (beispielsweise in Form einer Fusion oder Konsolidierung, oder in Form von Maßnahmen nach dem deutschen Umwandlungsgesetz), geben Sie HintHub ausdrücklich Ihre Zustimmung zur Übertragung Ihrer Daten an den neuen Eigentümer oder den Rechtsnachfolger, so dass wir weiterhin unsere Dienste bereitstellen können. Falls erforderlich, wird HintHub die betreffenden Datenschutzbehörden jeder Gerichtsbarkeit, in Übereinstimmung mit den Mitteilungsverfahren gemäß den geltenden Datenschutzgesetzen über eine solche Übertragung informieren.
* Wenn wir Ihre Zustimmung zur Offenlegung haben: Natürlich können jederzeit Situationen auftreten, für welche Sie uns, zusätzlich zu den oben aufgeführten, Ihre ausdrückliche Zustimmung zur Offenlegung Ihrer Daten gegenüber anderen gegeben haben. Dies kann beispielsweise der Fall sein, wenn Sie eine Referenz über Ihre HintHub-Erfahrung abgeben oder wenn wir, mit Ihrer Zustimmung, Ihre Kontaktdaten gegenüber Dritten offenlegen, damit diese Sie für Marketingzwecke kontaktieren können. Bitte lesen Sie in Abschnitt 5 unten weiter – der Rest dieser Datenschutzerklärung enthält wichtige Informationen, die auch für Sie gelten.
## 3. Datenschutz für Nutzer:innen
### 3.1 Wer sind Nutzer:innen?
Wenn Sie sich mit einem Hinthub-Account registrieren und/oder für eines unserer Abonnementmodelle eintragen, um die jeweils zur Verfügung stehenden Funktionen zu nutzen, sind sie Nutzer:in.
### 3.2 Welche Daten über Nutzer:innen sammeln wir?
Wenn Sie Nutzer:in sind, sammeln wir und verwenden Ihre Daten wie in Abschnitt 2 beschrieben. Zusätzlich sammeln wir die folgenden Daten über Sie, während Sie unseren Dienst nutzen:
   * Registrierungsinformationen: Sie benötigen einen HintHub-Account, bevor Sie das Hinweisgebersystem einrichten und einen oder mehrere interne Meldekanäle für Ihr Unternehmen erstellen können. Wenn Sie sich für einen Account registrieren, erfassen wir Ihren Vor- und Nachnamen, E-Mail-Adresse, Passwort, Firmenname, Firmenadresse sowie Ihre Rollenbezeichnung in Ihrem Unternehmen.
   * Rechnungsinformationen: Wenn Sie sich für einen unserer kostenpflichtigen Abonnement-Pläne entscheiden, werden wir Sie dazu auffordern, Ihre Rechnungsdetails zur Verfügung zu stellen, inklusive Ihres Rechnungsnamens, der Rechnungsadresse und zusätzlicher finanzieller Auskünfte, abhängig von der von Ihnen gewählten Zahlungsmethode und eventuellen lokalen Vorschriften Ihres Landes, Ihrer Region oder sonstigen geografisch-rechtlichen Einheit, in der Sie sich befinden bzw. Ihr Unternehmen seinen Sitz hat. Wir werden außerdem Daten über Ihr individuelles Abonnement und die zugehörigen Funktionen (inklusive dem Anmeldedatum und Verlängerungsdaten) speichern.
   * „Mein Account" Einstellungen: Sie können auf unserer Plattform verschiedene Präferenzen und persönliche Details über die Einstellung „Mein Account" sehen und bearbeiten. Sie können z.B. Präferenzen wie Ihre voreingestellte Sprache oder voreingestellte Zeitzone einstellen.
   * Hinweisdaten: Wir sammeln und speichern die Art und die Häufigkeit von Hinweisen, die Sie von Hinweisgeber:innen erhalten und werten diese Metadaten zur Verbesserung unserer Dienste sowie zu Reportingzwecken für Sie, die Nutzer:innen aus. Niemals werten wir die Hinweise selbst, sprich deren Inhalte aus, noch kann HintHub diese Inhalte seinen Mitarbeitenden oder Dritten offenlegen. Die Inhalte der Hinweise werden aber für die Nutzer:innen und ggf. für die jeweiligen Hinweisgeber:innen rechtssicher und für die gesetzlich vorgeschriebene Dauer gespeichert und sind nur durch die rechtmäßigen Nutzer:innen oder entsprechenden Hinweisgeber:innen darstellbar.
   * Persönliche Daten von Hinweisgeber:innen: HintHub erlaubt Ihnen, die persönlichen Daten Ihrer Hinweisgeber:innen zu sehen und zu nutzen, sofern die Hinweisgeber:innen dem vor Abgabe eines Hinweises zugestimmt haben. Wir speichern und verarbeiten diese Daten ausschließlich in Ihrem Namen und auf Ihre Anweisung hin. Rechtsgrundlage für die Datenverarbeitung ist Art. 6 Abs. 1 S. 1 lit. b DSGVO. Die weitere Nutzung von personenbezogenen Daten der Hinweisgeber:innen, insbesondere deren Weitergabe an öffentliche Stellen oder Strafverfolgungsbehörden liegt im alleinigen Ermessen und im Verantwortungsbereich der Nutzer:innen. Sollte die EU Whisteblower Directive oder lokale Gesetzgebungen, die aus dieser abgeleitet worden sind, nicht anderweitige Regelungen vorsehen, liegt die Verantwortung für die Daten von Hinweisgeber:innen allein bei den Nutzer:innen, denen sich die Hinweisgeber:innen ggf. mit personenbezogenen Daten bekannt machen.
### 3.3 Wie verwenden wir die Daten, die wir über Nutzer:innen sammeln?
Wir verwenden Daten von Nutzer:innen für dieselben Zwecke wie andere Website-Besucherinformationen, wie in Abschnitt 2 oben beschrieben. Zusätzlich verwenden wir Daten auch um:
   * unseren Dienst bereitzustellen: Wir verwenden Ihre Registrierungsdaten, Rechnungsdaten, „Mein Account" Einstellungen und Ihre Whistleblowermetadaten, um Ihnen unseren Dienst zur Verfügung zu stellen. Dies bedeutet, dass wir Ihnen auch unseren Kunden-Support zur Verfügung stellen, wofür wir Zugang zu Ihren Daten haben müssen, um Ihnen zu helfen. Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. b DSGVO.
   * Service-Analysen zu erstellen: Wir verwenden Ihre Daten in aggregierter Form, um Berichte oder Benchmarks zu erstellen. Dies bedeutet, dass wir Ihre Daten für Zwecke des Sicherheits- und Betriebsmanagements verwenden dürfen, um statistische Analysen zu erstellen, sowie für Forschungs- und Entwicklungszwecke. Bitte beachten Sie, dass diese Analysen, Berichte und Benchmarks nicht die Identifizierung einer Person zulässt oder keine Daten enthält, die dazu beitragen könnten, diese zu identifizieren. Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. f DSGVO, beruhend auf dem berechtigten Interesse an der Weiterentwicklung unseres Services.
   * Ihnen regelmäßig E-Mails zu schicken: Die E-Mail-Adresse, die Sie uns als Teil Ihrer Registrierungsinformationen zur Verfügung stellen, kann dazu verwendet werden, Ihnen Informationen und Updates betreffend Ihrer Bestellung zu senden. Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. b DSGVO.
   * um Sie zu Marketingzwecken zu kontaktieren: Zusätzlich und abhängig davon, ob Sie uns Ihre Zustimmung gegeben haben, können wir Ihre E-Mail-Adresse dazu verwenden, Ihnen gelegentlich Firmennews, Updates und zugehörige Produkt- oder Serviceinformationen sowie besonderen Angeboten zu schicken. Falls Sie sich zu einem bestimmten Zeitpunkt von zukünftigen E-Mails abmelden wollen, finden Sie detaillierte Abmeldeanweisungen am Ende einer jeden E-Mail, die wir Ihnen zusenden.
Bitte lesen Sie unten in Abschnitt 5 weiter – der Rest dieser Datenschutzerklärung enthält wichtige Informationen, die auch für Sie gelten.
## 4. Datenschutz für Hinweisgeber:innen
### 4.1 Wer ist Hinweisgeber:in?
Wenn Sie über eine unserer Webseiten oder die Webseite der Unternehmen oder Organisationen, denen unsere Nutzer:innen angehören, auf eine für das jeweilige Unternehmen oder der jeweiligen Organisation zugehörige Hinweisgeberportal gelangen, dass in der Regel unter Organisationsname.hinthub.eu erreichbar ist, und dort einen Hinweis mittels Abgabe von personenbezogenen Daten oder auf anonymisierende Weise abgeben, sind sie ein:e Hinweisgeber:in
### 4.2 Welche Daten sammeln wir über Hinweisgeber:innen?
Wir weisen explizit darauf hin, dass HintHub nicht als Dritter im Sinne (54) Richtlinie (EU) 2019/1937 zu verstehen ist und damit nicht als entgegennehmende Stelle für Hinweise angesehen werden kann. Damit schließen wir auch eine Auftragsdatenverarbeitung für Nutzer:innen und eine Verantwortung für den Umgang der Nutzer:innen mit abgegebenen Hinweisen durch Hinweisgeber:innen sowie der Nutzung oder Weitergabe der personenbezogenen Daten letzterer aus.
Sofern Sie als Hinweisgeber:in personenbezogene Daten abgeben, sammeln wir die folgenden Daten:
   * Metadaten über die Abgabe von Hinweisen: Wir sammeln, speichern und analysieren die Häufigkeit von Hinweisen und setzen diese in Verbindung mit der Plausibilität dieser, insbesondere um schadhafte Nutzung unserer Systeme zu verhindern und Schaden von den Nutzer:innen abzuwenden.
   * E-Mail-Adresse: HintHub zeichnet die E-Mail-Adresse von Hinweisgeber:innen auf, wenn diese zur Verfügung gestellt wird, um Ihnen automatisiert weitere Hinweise zur Verarbeitung Ihres Hinweises durch ein:e Nutzer:in zusenden zu können. Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. b, Art. 28 Abs. 3 DSGVO in Verbindung mit der Richtlinie (EU) 2019/1937 des Europäischen Parlaments und des Rates vom 23. Oktober 2019 zum Schutz von Personen, die Verstöße gegen das Unionsrecht melden. Natürlich können Sie jederzeit vom Opt-out Gebrauch machen, indem Sie die Abmeldeanweisungen am Ende einer jeden E-Mail, die sie von uns erhalten, befolgen.
### 4.3 Wie verwenden wir die Daten, die wir über Hinweisgeber:innen sammeln?
Wir verwenden Daten über Hinweisgeber:innen in aggregierter Form, um Berichte oder Benchmarks zu erstellen. Dies bedeutet, dass wir Ihre Daten für Zwecke des Sicherheits- und Betriebsmanagements verwenden dürfen, um statistische Analysen zu erstellen, sowie für Forschungs- und Entwicklungszwecke. Bitte beachten sie, dass diese Analysen nicht die Identifizierung einer Person zulassen oder Daten enthalten, die dazu beitragen könnten, diese zu identifizieren. Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. f DSGVO, beruhend auf dem berechtigten Interesse an der Weiterentwicklung unseres Services.
## 5. Verantwortliche und Datenverarbeiter
HintHub ist "Verantwortlicher" im Sinne des Art. 4 DSGVO für alle Website-Besucher- und Ersteller-Informationen, die wir erfassen. Der Verantwortliche für die Hinweisdaten, die von Hinweisgeber:innen geliefert werden, ist jedoch der/die Nutzer:in. Die Nutzer:innen entscheiden im eigenen Ermessen bzw. unter der Maßgabe geltender europäischer oder nationaler Gesetze, wie sie mit personenbezogenen Daten- und Hinweisdaten umgehen. Falls Sie Fragen zu einem abgegebenen Hinweis, die Nutzung Ihrer Hinweise oder ihrer personenbezogenen Daten haben, kontaktieren Sie bitte direkt die jeweiligen Nutzer:innen.
## 6. Wie lange bewahren wir Ihre Daten auf?
Wir werden Ihre Daten, sofern erhoben, in Übereinstimmung mit den geltenden gesetzlichen Aufbewahrungsfristen aufbewahren. Nach Ablauf dieser Frist werden die entsprechenden Daten, sofern sie zur Erfüllung des Vertrages zwischen Ihnen und uns nicht länger notwendig sind, routinemäßig gelöscht.
Wenn Sie ein Nutzer:in sind, werden, wenn Sie Ihren Account löschen, all Ihre Daten, soweit dies gesetzlich erlaubt ist, aus unserem System gelöscht. Bitte beachten Sie, dass dieser Inhalt nicht mehr hergestellt werden kann, wenn Ihr Account gelöscht ist. Wir haften nicht für Verlust oder Schaden nach dem Löschen Ihres Accounts oder als Resultat davon. Es liegt in Ihrer Verantwortung, sicherzustellen, dass Inhalte, die Sie benötigen, gesichert wurden bzw. vor der Löschung kopiert wurden.
Ungeachtet des oben Gesagten bewahren und verwenden wir Ihre Daten, um unsere gesetzlichen Verpflichtungen zu erfüllen (inklusive der gesetzlichen Aufbewahrungsfristen), um Streitigkeiten beizulegen oder um unsere vertraglichen Vereinbarungen durchzusetzen. Wir dürfen Ihre Daten zudem in dem Umfang bewahren und verwenden, wie solche Daten in unseren statistischen Analysen, Berichten und Benchmarks integriert sind, und wir werden Ihre Daten nicht in einer Form integrieren, die eine Identifikation zulässt oder vernünftigerweise dazu beitragen kann, eine Person zu identifizieren.
## 7. Wie Website-Besucher:innen ihre Daten kontrollieren können
### 7.1 Ihre Rechte als betroffene Person im Allgemeinen
Ihre Rechte in Bezug auf unsere Verarbeitung Ihrer personenbezogenen Daten umfasst Folgendes:
   * Auskunft: Sie können von uns jederzeit kostenlos eine Bestätigung darüber verlangen, ob Sie betreffende personenbezogene Daten verarbeitet werden; ist dies der Fall, so haben Sie ein Recht auf Auskunft über diese personenbezogenen Daten (siehe Abschnitt 7.2 unten für mehr Details).
   * Berichtigung: Wenn Sie der Ansicht sind, dass Ihre personenbezogenen Daten, die wir speichern oder verarbeiten, fehlerhaft oder unvollständig sind, können Sie eine Berichtigung dieser fehlerhaften oder unvollständigen Daten verlangen (siehe Abschnitt 7.2 unten für mehr Details).
   * Recht auf Löschung (Recht auf Vergessenwerden): Unter bestimmten Bedingungen können Sie jederzeit die Löschung Ihrer personenbezogenen Daten verlangen (siehe Abschnitt 7.3 für mehr Details).
   * Recht auf Einschränkung der Verarbeitung: Unter bestimmten Bedingungen können Sie von uns eine Einschränkung der Verarbeitung verlangen (siehe Abschnitt 7.4 für mehr Details).
   * Widerspruch: Sie können jederzeit aus Gründen basierend auf Ihrer besonderen Situation einen Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten einlegen (siehe Abschnitt 7.5 für mehr Details).
   * Recht auf Datenübertragbarkeit: Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die Sie uns zur Verfügung gestellt haben, in einem strukturierten, gängigen maschinenlesbaren Format zu erhalten (siehe Abschnitt 7.6 für mehr Details).
   * Widerruf: Wenn unsere Verarbeitung Ihrer personenbezogenen Daten auf Ihrer Einwilligung basiert, können Sie diese jederzeit wiederrufen, ohne dass dies die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung berührt (siehe Abschnitt 7.7 für mehr Details).
   * Beschwerde: Wenn Sie der Ansicht sind, dass unsere Datenverarbeitungsmaßnahmen oder –mittel gegen die DSGVO verstoßen, können Sie eine Beschwerde bei einer Aufsichtsbehörde einreichen (siehe Abschnitt 7.8 für mehr Details).
### 7.2 Kann ich auf meine personenbezogenen Daten zugreifen und sie korrigieren?
Ja. Sie haben das Recht, jederzeit Auskunft über die Sie betreffenden personenbezogenen Daten, die HintHub über Sie verarbeitet, zu erhalten, und Sie können auch von uns verlangen, diese Daten zu berichtigen, zu löschen, einzuschränken oder zu sperren, falls erforderlich. Sie können diese Rechte jederzeit ausüben, indem Sie unseren Datenschutz-Support unter [privacy@hinthub.eu](mailto:privacy@hinthub.eu) kontaktieren.
Wenn Sie Nutzer:in sind, können Sie Ihre personenbezogenen Daten ändern, indem Sie sich einloggen und unter Ihren Einstellungen auf der „Mein Account"-Seite und der „Plan + Abrechnung"-Seite die zur Verfügung gestellten Anweisungen befolgen, oder indem Sie unsere „Contact us"-Seite besuchen. Wir empfehlen Ihnen, Ihre personenbezogenen Daten sofort zu aktualisieren, wenn diese sich ändern.
Wenn Sie Hinweisgeber:in sind, beachten Sie bitte, dass Ihre personenbezogenen Daten exklusiv von der jeweiligen Nutzer:in verwaltet und kontrolliert werden, mit der Sie über HintHub Kontakt zur Abgabe von Hinweisen aufgenommen haben. Wenn Sie Ihre personenbezogenen Daten überprüfen, berichtigen, löschen, einschränken oder sperren möchten, bitten wir Sie darum, direkt den/die jeweilige/n Nutzer:in zu kontaktieren.
### 7.3 Was tun, wenn ich nicht will, dass meine personenbezogenen Daten verarbeitet werden?
Wenn Sie nicht möchten, dass Ihre personenbezogenen Daten verarbeitet werden, haben Sie das Recht, von uns zu verlangen, dass die Sie betreffenden personenbezogenen Daten unverzüglich gelöscht werden, sofern einer der folgenden Gründe zutrifft:
   * Die persönlichen Daten wurden zu Zwecken gesammelt oder in einer anderen Art und Weise verarbeitet, für die sie nicht länger notwendig sind;
   * Die personenbezogenen Daten sind für die Zwecke, für die sie erhoben oder auf sonstige Weise verarbeitet wurden, nicht mehr notwendig. * Sie widerrufen Ihre Einwilligung, auf die sich die Verarbeitung gemäß Artikel 6 Absatz 1 Buchstabe a oder Artikel 9 Absatz 2 Buchstabe a DSGVO stützte, und es fehlt an einer anderweitigen Rechtsgrundlage für die Verarbeitung.
Sie legen gemäß Artikel 21 Absatz 1 DSGVO Widerspruch gegen die Verarbeitung ein und es liegen keine vorrangigen berechtigten Gründe für die Verarbeitung vor, oder Sie legen gemäß Artikel 21 Absatz 2 DSGVO Widerspruch gegen die Verarbeitung ein.
   * Die personenbezogenen Daten wurden unrechtmäßig verarbeitet.
   * Die Löschung der personenbezogenen Daten ist zur Erfüllung einer rechtlichen Verpflichtung nach dem Unionsrecht oder dem Recht der Mitgliedstaaten erforderlich, dem wir unterliegen.
   * Die persönlichen Daten wurden in Bezug auf angebotene Dienste Informationsgesellschaft gemäß Artikel 8 Abs. 1 DSGVO erhoben.
Wenn einer der oben genannten Gründe zutrifft und Sie wünschen, dass Ihre personenbezogenen Daten gelöscht werden, kontaktieren Sie bitte unseren Datenschutz-Support unter [privacy@hinthub.eu](mailto:privacy@hinthub.eu). Wir werden alles veranlassen, um dem Löschungsantrag unverzüglich nachzukommen.
### 7.4 Kann ich die Verarbeitung meiner Daten einschränken?
Ja. Sie können von uns eine sogenannte „Einschränkung der Verarbeitung" verlangen, wenn einer der folgenden Voraussetzungen zutrifft:
      * Sie bestreiten die Richtigkeit der personenbezogenen Daten, und zwar für eine Dauer, die es uns ermöglicht, die Richtigkeit der personenbezogenen Daten zu überprüfen;
      * die Verarbeitung Ihrer personenbezogenen Daten ist unrechtmäßig und Sie lehnen die Löschung der personenbezogenen Daten ab und verlangen stattdessen die Einschränkung der Nutzung der personenbezogenen Daten;
      * Wir benötigen Ihre personenbezogenen Daten nicht länger für die Zwecke der Verarbeitung, Sie benötigen sie jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen;
      * Sie haben Widerspruch gegen die Verarbeitung gemäß Artikel 21 Abs. 1 DSGVO eingelegt, solange noch nicht feststeht, ob unsere berechtigten Gründe gegenüber Ihren Gründen überwiegen.
Wenn eine der vorhergehend genannten Bedingungen vorliegt und Sie die Einschränkung der Verarbeitung Ihrer personenbezogenen, von uns gespeicherten, Daten wünschen, können Sie uns jederzeit kontaktieren, um die Einschränkung der Verarbeitung zu veranlassen.
### 7.5 Kann ich der Verarbeitung meiner personenbezogenen Daten widersprechen?
Ja. Sie haben das Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten, die aufgrund von Artikel 6 Absatz 1 Buchstaben e oder f DSGVO erfolgt, Widerspruch einzulegen. In diesem Fall verarbeiten wir die personenbezogenen Daten nicht mehr, es sei denn, wir weisen zwingende schutzwürdige Gründe gemäß Artikel 21 Abs. 1 DSGVO nach oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.
Wenn wir Ihre personenbezogenen Daten verarbeiten, um Direktwerbung zu betreiben, haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten zum Zwecke derartiger Werbung einzulegen; dies gilt auch für das Profiling (individuelle Profilerstellung), soweit es mit solcher Direktwerbung in Verbindung steht. Wenn Sie der Verarbeitung zu Direktmarketingzwecken widersprechen, werden wir Ihre personenbezogenen Daten nicht länger für diese Zwecke verarbeiten.
Wenn wir personenbezogene Daten für wissenschaftliche oder historische Forschungszwecke oder für statistische Zwecke gemäß Artikel 89 (1) DSGVO verarbeiten, haben Sie das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten Widerspruch einzulegen, es sei denn, die Verarbeitung ist zur Erfüllung einer im öffentlichen Interesse liegenden Aufgabe erforderlich.
### 7.6 Kann ich meine Daten an einen anderen Verantwortlichen übertragen lassen?
Ja. Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die Sie uns bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten, und Sie haben das Recht, diese Daten ohne eine Behinderung durch uns an einen anderen Verantwortlichen zu übermitteln, sofern die Verarbeitung auf einer Einwilligung gemäß Artikel 6 Abs 1 lit. a oder Artikel 9 Abs 2 lit a DSGVO oder auf einem Vertrag gemäß Artikel 6 Abs 1 lit b DSGVO beruht und die Verarbeitung mithilfe automatisierter Verfahren erfolgt.
Bitte beachten Sie, dass diese Übertragung verboten sein könnte, wenn die Verarbeitung zur Durchführung eines Auftrages des öffentlichen Interesses oder der HintHub verliehenen Ausübung öffentlicher Gewalt notwendig sind. Bei der Ausübung Ihres Rechts auf Datenübertragbarkeit haben Sie auch das Recht darauf, dass Ihre personenbezogenen Daten direkt von uns an einen anderen Verantwortlichen übertragen werden, soweit dies technisch möglich ist.
### 7.7 Was passiert wenn ich meine Meinung ändere – kann ich meine Einwilligung widerrufen?
Ja. Wie in Kapitel 1.2 festgesetzt, müssen Sie dieser Datenschutzerklärung, unseren Allgemeinen Geschäftsbedingungen und unserer Auftragsverarbeitungsvereinbarung zustimmen, um die Erlaubnis zu erhalten, unseren Dienst zu nutzen. Wenn Sie jedoch unseren Dienst nicht länger nutzen wollen, haben Sie jederzeit das Recht, Ihre Einwilligung zur Verarbeitung der Sie betreffenden personenbezogenen Daten zu widerrufen. Hierfür kontaktieren Sie bitte unseren Datenschutz-Support unter [privacy@hinthub.eu](mailto:privacy@hinthub.eu). Wenn Sie Ihre Einwilligung widerrufen, können Sie unsere Dienste nicht mehr nutzen. Bitte beachten Sie, dass der Widerruf die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt.
### 7.8 Alles ging schief – was kann ich tun?
Wenn Sie der Ansicht sind, dass unsere Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die DSGVO verstößt, unzureichend sind oder Sie andere für Sie zutreffende Gründe haben, haben Sie gemäß Artikel 77 DSGVO das Recht, eine Beschwerde bei einer Aufsichtsbehörde einzureichen.
## 8. Datensicherheit
### 8.1 Wo werden Ihre Daten gespeichert?
Die Server, auf denen Ihre Daten und Informationen gespeichert sind, befinden sich in Deutschland, einem Mitgliedstaat der Europäischen Union, oder einem anderen Unterzeichner des Abkommens über den Europäischen Wirtschaftsraum.
### 8.2 Wie sichern wir Ihre Daten?
Wir sind dazu verpflichtet, Ihre personenbezogenen Daten mit äußerster Sorgfalt zu handhaben. Zu diesem Zweck haben wir verschiedene technische und organisatorische Maßnahmen eingeführt und halten diese auch aufrecht. Diese Maßnahmen dienen dazu, Ihre Daten gegen unberechtigten Verlust, Zerstörung, Änderung, Offenlegung oder Zugriff und gegen alle gesetzeswidrigen Formen der Verarbeitung zu schützen.
### 8.3 Besteht das Risiko, dass meine Daten dennoch von dritten Parteien eingesehen werden können?
Leider ja. Ungeachtet der Sicherheitsschutzmaßnahmen und Vorkehrungen, die wir treffen, besteht immer ein Risiko, dass Ihre personenbezogenen Daten, als Ergebnis der Sammlung und Übertragung durch das Internet, durch dritte Parteien eingesehen und verwendet werden können. Wenn Sie Fragen zur Sicherheit Ihrer personenbezogenen Daten haben, kontaktieren Sie bitte unseren Kundendienst über die „Contact Us" Seite.
## 9. Welche Rechtsgrundlage liegt der Datenverarbeitung zugrunde?
Bei der Verarbeitung Ihrer persönlichen Daten handelt HintHub jederzeit in Übereinstimmung mit dem gültigen Gesetz. Wir haben zu jeder Datenverarbeitung die passende Rechtsgrundlage benannt. Im Folgenden erklären wir, was diese bedeuten:
Datenverarbeitungsvorgänge, die sich auf Artikel 6 Abs 1 lit a DSGVO stützen, sind rechtmäßig, wenn die betroffene Person ihre Einwilligung zu der Verarbeitung für einen oder mehrere bestimmte Zwecke gegeben hat.
Wenn die Verarbeitung personenbezogener Daten für die Erfüllung eines Vertrages, dessen Vertragspartei Sie sind oder zur Durchführung vorvertraglicher Maßnahmen erforderlich ist, basiert unsere Verarbeitung auf Artikel 6 Abs 1 lit b DSGVO. Vorvertragliche Maßnahmen sind dabei weit zu fassen und können schon das Surfen auf unserer Webseite darstellen.
Wenn wir einer rechtlichen Verpflichtung unterliegen, die die Verarbeitung von personenbezogenen Daten erforderlich macht (wie z.B. die Erfüllung von Steuerverpflichtungen), basiert unsere Verarbeitung auf Artikel 6 Abs 1 lit c DSGVO.
In sehr seltenen Fällen kann die Verarbeitung von personenbezogenen Daten durch uns erforderlich sein, um lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person zu schützen; in diesen Fällen basiert unsere Verarbeitung auf Artikel 6 Abs. 1 lit d DSGVO.
Gegenwärtig führen wir keine Aufgaben durch, die im öffentlichen Interesse liegen oder die in Ausübung öffentlicher Gewalt erfolgen; dies kann jedoch in der Zukunft stattfinden und in diesem Fall basiert unsere Verarbeitung auf Artikel 6 Abs 1 lit e DSGVO.
Schließlich kann die Verarbeitung von personenbezogenen Daten durch uns auf Artikel 6 Abs 1 lit f DSGVO basieren. Diese gesetzliche Basis wird für Verarbeitungsvorgänge verwendet, die nicht von einem der oben genannten Gründe abgedeckt sind, wenn die Verarbeitung zur Wahrung berechtigter Interessen von uns oder eines Dritten erforderlich ist, sofern nicht die Interessen oder Grundrechte und Grundfreiheiten der betroffenen Person, die den Schutz personenbezogener Daten erfordern, überwiegen (der Maßstab ist strenger, wenn es sich bei der betroffenen Person um ein Kind handelt).
Dort, wo unsere Verarbeitung von personenbezogenen Daten auf Artikel 6 Abs 1 lit f DSGVO basiert, sind unsere rechtmäßigen Interessen kurz skizziert; dies umfasst insbesondere das Betreiben unserer Website, die Bereitstellung unserer Dienste, die Personalisierung der Erfahrung unserer Website-Besucher und die Erfüllung ihrer individuellen Bedürfnisse (inklusive der ständigen Verbesserung unserer Website und unserer Kundendienste) sowie das Interesse, Sie zu Marketingzwecken zu kontaktieren (wenn Sie uns dazu Ihre Einwilligung gegeben haben).
## 10. Verwendet HintHub eine automatische Entscheidungsfindung?
Nein. Als verantwortungsbewusstes Unternehmen verwendet HintHub keine automatische Entscheidungsfindung.
## 11. Änderungen dieser Datenschutzerklärung
HintHub behält sich das Recht vor, diese Datenschutzerklärung jederzeit zu ändern, und zwar unter Mitteilung des Änderungsdatums auf dieser Seite oder an einer anderen Stelle der Website. Wir empfehlen Ihnen dringend, diese Seite in Bezug auf das Datum der letzten Änderung häufiger zu überprüfen. Wenn Sie Änderungen an dieser Datenschutzerklärung ablehnen, müssen Sie die Nutzung von HintHub beenden.
## 12. Wie kontaktieren Sie uns?
Hier sind unsere Kontaktinformationen (gemäß Artikel 4 Paragraf 7 der Europäischen Datenschutzgrundverordnung DSGVO):
HintHub GmbH, Hauptstr. 28, 15806 Zossen, Deutschland, [privacy@hinthub.eu](mailto:privacy@hinthub.eu)
 
 
 
 
## Anlage 1 zur Datenschutzerklärung
Technische und organisatorische Maßnahmen
HintHub sichert zu, folgende technische und organisatorische Maßnahmen getroffen zu haben, insofern HintHub darauf direkten Einfluss nehmen kann:
      1. Maßnahmen zur Sicherung der Vertraulichkeit
### 1.1. Zutrittskontrolle
Maßnahmen, die unbefugten Personen den Zutritt zu IT-Systemen und Datenverarbeitungsanlagen mit denen personenbezogene Daten verarbeitet werden, sowie vertraulichen Akten und Datenträgern physisch verwehren. Beschreibung des Zutrittskontrollsystems:
      * Schlösser an den Eingangstüren
      * Sorgfältige Auswahl von Reinigungspersonal
      * Kontrollierte Schlüsselvergabe
      * Zugangs-Management: autorisiertes Personal und Umfang der Autorisierung sind vordefiniert 
      * Maßnahmen der Dienstleister aus Anlage 2
### 1.2. Zugangskontrolle
Maßnahmen, die verhindern, dass Unbefugte datenschutzrechtlich geschützte Daten verarbeiten oder nutzen können. 
Beschreibung des Zugangskontrollsystems:
      * System und Datenzugang sind eingeschränkt auf autorisierte Nutzer
      * Nutzer müssen sich mit Nutzername und Passwort identifizieren
      * All logins/logoffs werden aufgezeichnet
      * Einsatz einer zentralen Passwort-Policy
### 1.3. Zugriffskontrolle
Maßnahmen, die gewährleisten, dass die zur Benutzung der Datenverarbeitungsverfahren Berechtigten ausschließlich auf die ihrer Zugriffsberechtigung unterliegenden personenbezogenen Daten zugreifen können, so dass Daten bei der Verarbeitung, Nutzung und Speicherung nicht unbefugt gelesen, kopiert, verändert oder entfernt werden können.
Beschreibung des Zugriffskontrollsystems:
      * System und Datenzugang sind eingeschränkt auf autorisierte Nutzer
      * Nutzer müssen sich mit Nutzername und Passwort identifizieren
      * Alle Datenzugänge werden automatisch aufgezeichnet
      * Eine kleine Anzahl an System Administratoren
      * Protokollierung von Zugriffen und Missbrauchsversuchen
### 1.4. Trennungsgebot
Maßnahmen, die gewährleisten, dass zu unterschiedlichen Zwecken erhobene Daten getrennt verarbeitet werden und so von anderen Daten und Systemen getrennt sind, dass eine ungeplante Verwendung dieser Daten zu anderen Zwecken ausgeschlossen ist.
Beschreibung des Trennungskontrollvorgangs:
      * Systeme erlauben Daten Segregation durch unterschiedliche Software
      * Produktiv- und Testsysteme sind getrennt voneinander
      * Datensätze sind nur durch Systeme zugänglich, die vordefiniert sind
      * Datenbanken Nutzerrechte werden zentral ausgegeben und verwaltet 
### 1.5. Pseudonymisierung
Maßnahmen, die den unmittelbaren Personenbezug während der Verarbeitung in einer Weise reduzieren, dass nur mit Hinzuziehung zusätzlicher Informationen eine Zuordnung zu einer spezifischen betroffenen Person möglich ist. Die Zusatzinformationen sind dabei durch geeignete technische und organisatorische Maßnahmen von dem Pseudonym getrennt aufzubewahren.
      * Keine, da auf einem Zentral-Server verarbeitet wird
      1. Maßnahmen zur Sicherung der Integrität
### 2.1. Weitergabekontrolle
Maßnahmen, die gewährleisten, dass personenbezogene Daten bei der elektronischen Übertragung oder während ihres Transports oder ihrer Speicherung auf Datenträger nicht unbefugt gelesen, kopiert, verändert oder entfernt werden können sowie Maßnahmen mit denen überprüft und festgestellt werden kann, an welche Stellen eine Übermittlung personenbezogener Daten vorgesehen ist. Beschreibung der Weitergabekontrolle:
      * HTTPS - Unnötige Ausdrucke und Fehldrucke werden vernichtet
      * Keine Benutzung von physischen Datenträgern
      * Umfassende Logging Prozeduren
      * Private Datenträger vom Personal dürfen nicht bei der Arbeit genutzt werden
### 2.2. Eingabekontrolle
Maßnahmen, die gewährleisten, dass nachträglich überprüft und festgestellt werden kann, ob und  von wem personenbezogene Daten in DV-Systeme eingegeben, verändert oder entfernt worden sind.
Beschreibung des Eingabekontrollvorgangs:
      * Logging von allen System Aktivitäten und Behalten der Logs für mindestens 6 Monate
      * Nutzung der zentralen Rechteverwaltung für die Eingabe, Ändern und Löschen von Daten
      1. Maßnahmen zur Sicherung der Verfügbarkeit und Belastbarkeit
### 3.1. Verfügbarkeitskontrolle
Maßnahmen, die sicherstellen, dass personenbezogene Daten gegen zufällige Zerstörung oder Verlust geschützt sind.  
Beschreibung des Verfügbarkeitskontrollsystems:
      * Es werden regelmäßig Backups erstellt.
      * Backup- und Wiederherstellungsplan ist vorhanden.
      * Datensicherungsdateien werden an einem sicheren und entfernten Ort gespeichert.
      * Lokalisierung - Datenrettung wird regelmäßig getestet
      * Sowie diverse weitere Maßnahmen der Server Dienstleister
### 3.2. Rasche Widerherstellbarkeit
Maßnahmen, die die Fähigkeit sicherstellen, die Verfügbarkeit der personenbezogenen Daten und den Zugang zu ihnen bei einem physischen oder technischen Zwischenfall rasch wiederherzustellen.
Beschreibung der Maßnahmen:
      * Datensicherungsverfahren
      1. Maßnahmen zur regelmäßigen Evaluation der Sicherheit der Datenverarbeitung
Maßnahmen, die die datenschutzkonforme und sichere Verarbeitung sicherstellen.
Beschreibung der Überprüfungsverfahren:
      * Formalisierter Prozess für den Fall eines Datenschutzvorfalls
