# Privacy Policy
## 1. Your privacy is important to us
### 1.1 Who are we?
HintHub GmbH (hereinafter referred to as "HintHub" or "we") provides a whistleblower software-as-a-service platform of the same name that enables users to establish a reporting channel through which whistleblowers can share information with the users or the associated companies or organisations whose content potentially or factually indicates violations of internal or external regulations, laws, codes or other standards of these respective companies or organisations. If you would like to speak to us directly, you will find our contact details at the end of this Privacy Policy. We have not appointed an internal or external data protection officer, as we are not obliged to do so by German and European law due to the size of our company.

### 1.2 What is the purpose of this privacy policy?
This Privacy Policy describes how HintHub collects, uses, stores, shares and secures your personal information. It applies when you access, visit or use any part of our website or service. You can access, download, save and print this Privacy Policy at any time by visiting our website at hinthub.eu/privacy. Depending on how you use our site or service, some parts of this statement may apply to you and some parts may not - each section will clearly indicate whether it applies to you. Please read this Privacy Policy and our Terms and Conditions carefully. Your agreement to these terms is required to use our service. You cannot use our service if you do not agree to these terms.

### 1.3 What does "personal data" mean?
In this privacy statement we will talk a lot about "personal data". This term is defined in Article 4 of the European Data Protection Regulation (GDPR) as follows and is used in this sense for the purposes of this privacy statement: "personal data means any information relating to an identified or identifiable natural person (hereinafter 'data subject'); an identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, economic, cultural or social identity of that natural person. "

### 1.4 Website visitors, users and whistleblowers
HintHub is used by website visitors (people who visit our website, including general visitors who do not register with an account with us), users (people who operate a reporting channel for their business through our website) and whistleblowers (people who provide pre-specified information). How we store and handle your information depends in large part on whether you are a website visitor, user and/or whistleblower. For this reason, we have divided this privacy policy into three sections.

## 2. Data protection for website visitors
### 2.1 Who are website visitors?
If and while you visit our website hinthub.eu (even without registering for an account) you are a "website visitor".

### 2.2 What data do we collect from website visitors?
While you are visiting our website, we collect the following data in the background from all our website visitors: 



Statistical usage data: We collect and analyse data about how you found our website, how you browse, how long you stay and what you click on during your visit. 

Device and browser data: We collect data about the device used to access our website (e.g. whether you are using a laptop or smartphone, its manufacturer and the operating system you are using) and the application (e.g. whether you are using Chrome or Firefox). This also includes your public IP address, which allows us to draw conclusions about your geographical location. 

Referral Data: If you arrived at our website through an external referral (such as via a link from another website or an email) we store information about the source that referred you to us.

Data from "webhooks" or "page tags": We use so-called "tracking services" from third-party providers or ourselves that use webhooks and page tags (also known as web beacons or web bugs) to collect aggregated and anonymised data about visitors to our websites. This data may include both usage and website visitor statistics.



If you interact with our website in certain ways, we will also collect the following data about you:  

Voluntary data: We may collect additional personal information or data from you if you voluntarily provide it to us in other contexts, such as testimonials or public content. The legal basis for collecting the data is Art. 6 para. 1 p. 1 lit. f GDPR, based on the interest in enabling you to visit the website and ensuring the continued functionality and security of our systems. The legal basis for the collection of voluntary data is Art. 6 para. 1 p. 1 lit. b GDPR.

### 2.3 How do we use the data collected about website visitors?
We use the information we collect about website visitors to:personalise your experience: Your data helps us to better meet your individual needs.Improve our website: We constantly strive to improve our web offerings based on the data and feedback we receive from you.Improve customer service: Your data helps us to respond more effectively to your customer service requests and support needs and to evaluate or develop new features. The legal basis is Art. 6 para. 1 p. 1 lit. f DSGVO, based on the legitimate interest in increasing the attractiveness of our service.

### 2.4 When and with whom do we share your data?
HintHub recognises that you have entrusted us with safeguarding the confidentiality of your information. Your trust is very important to us, so we will only disclose or share your information in limited circumstances and in accordance with applicable law. In particular, we may disclose your information to:

our service providers. HintHub uses third party service providers to assist in providing our services to you. In particular, these include credit card and payment processors, data hosting service providers and providers of web analytics tools. We give these providers access to your data, but only to the extent they need to perform their services for us. We also contractually require these service providers to keep your data private and only use it to provide your services. If our service providers process your data outside the European Union, this may mean that your data is transferred to a country with a lower data protection standard than the European Union. In such cases, HintHub will ensure that the relevant service providers provide, contractually or otherwise, an equivalent level of data protection.

The service providers we use are:

Sendinblue GmbH, Köpenicker Straße 126, 10179 Berlin, Germany. For more information, see General Terms of Service of Sendinblue Services - Sendinblue and Privacy Overview: Everything You Need to Know!

Chargebee Inc, 340 South Lemon Avenue Suite 1537 Los Angeles, CA 91789 United States. For more information, see Terms of Service - Chargebee Inc, Privacy Policy - Chargebee Inc and EU-GDPR - Chargebee Docs

Hetzner Online GmbH, Industriestr. 25, 91710 Gunzenhausen, Germany. For more information, see Legal Notice - Hetzner Online GmbH and Data Privacy - Hetzner Online GmbH

Matomo, InnoCraft, 7 Waterloo Quay PO625, 6140 Wellington, New Zealand. For more information, see 100% Data Ownership - Matomo Analytics , Matomo Privacy Policy and GDPR - Matomo Analytics

Sentry, Functional Software, Inc., 45 Fremont Street, 8th Floor, San Francisco, CA 94105 United States.Terms of Service 1.2.0 (February 5, 2020) , Privacy Policy 2.2.1 (November 10, 2020) and Data Security, Privacy, and Compliance Overview

Cloudflare Inc., 101 Townsend St., San Francisco, California 94107, United States. Terms of Use | Cloudflare and Cloudflare Privacy Policy | Cloudflare 

Where required or permitted by law: In limited exceptional circumstances, public authorities (such as courts, government agencies, prosecutors, antitrust authorities and others) may require us to disclose information to them in the course of their duties (for example, to investigate, prevent or act on illegal activity). We may disclose your information if required or permitted by law. We may also do so if we believe disclosure is necessary to protect our rights, your safety or the safety of others, and/or to comply with a judicial proceeding, court order, subpoena or other legal process served on us. 

If there is a change in the shareholder or ownership structure of HintHub: If there is a change in the shareholder structure of our company or in any material part of our company (either through a share deal or an asset deal), or if we undergo a restructuring (for example, in the form of a merger or consolidation, or in the form of measures under the German Transformation Act), you expressly give HintHub your consent to transfer your information to the new owner or legal successor so that we can continue to provide our services. If necessary, HintHub will notify the relevant data protection authorities of each jurisdiction, in accordance with the notification procedures under applicable data protection laws, of such transfer.

If we have your consent to disclose: Of course, situations may arise at any time for which, in addition to those listed above, you have given us your explicit consent to disclose your information to others. This may be the case, for example, when you provide a testimonial about your HintHub experience or when, with your consent, we disclose your contact details to third parties so that they can contact you for marketing purposes. Please continue reading in section 5 below - the rest of this Privacy Policy contains important information that also applies to you.
## 3. Data protection for users
### 3.1 Who are users?
When you register with a Hinthub account and/or sign up for one of our subscription plans to use the respective features available to you, you are a User.

### 3.2 What data do we collect about users?
If you are a User, we collect and use your information as described in Section 2. In addition, we collect the following information about you while you are using our service:



Registration Information: You will need a HintHub account before you can set up the whistleblower system and create one or more internal reporting channels for your organisation. When you register for an account, we collect your first and last name, email address, password, company name, company address, and your role title in your company.



Billing Information: If you choose one of our paid subscription plans, we will ask you to provide your billing details, including your billing name, billing address and additional financial information, depending on the payment method you choose and any local regulations of your country, region or other geographical/legal entity in which you are located or your business is based. We will also store information about your individual subscription and associated features (including the sign-up date and renewal dates). 



"My Account" preferences: You can view and edit various preferences and personal details on our platform via the "My Account" setting. For example, you can set preferences such as your default language or default time zone.



Notice Data: We collect and store the type and frequency of tips you receive from tipsters and use this metadata to improve our services and for reporting purposes to you, the user. We never evaluate the tips themselves, i.e. their contents, nor can HintHub disclose these contents to its employees or third parties. However, the contents of the notices are stored in a legally secure manner for the users and, if applicable, for the respective whistleblowers for the legally prescribed period of time and can only be displayed by the legitimate users or corresponding whistleblowers.



Personal data of whistleblowers: HintHub allows you to view and use the personal data of your whistleblowers, provided that the whistleblowers have consented to this before submitting a whistleblower. We store and process this data exclusively on your behalf and at your instruction. The legal basis for data processing is Art. 6 para. 1 sentence 1 lit. b GDPR. The further use of personal data of whistleblowers, in particular their disclosure to public authorities or law enforcement agencies, is at the sole discretion and responsibility of the user. If the EU Whistleblower Directive or local legislation derived from it does not provide for other regulations, the responsibility for the data of whistleblowers lies solely with the users to whom the whistleblowers may disclose personal data.

### 3.3 How do we use the data we collect about users?
We use user information for the same purposes as other website visitor information, as described in section 2 above. In addition, we also use data to: 



provide our service: We use your registration data, billing data, "My Account" settings and your whistleblower metadata to provide our service to you. This means that we also provide you with our customer support, for which we need to have access to your data in order to help you. The legal basis is Art. 6 para. 1 p. 1 lit. b DSGVO.



service analytics: We use your data in aggregated form to create reports or benchmarks. This means that we may use your data for security and operational management purposes, to create statistical analyses, and for research and development purposes. Please note that these analyses, reports and benchmarks do not identify an individual or contain any data that could help to identify them. The legal basis is Art. 6 para. 1 p. 1 lit. f DSGVO, based on the legitimate interest in the further development of our service.



to send you e-mails on a regular basis: The email address you provide to us as part of your registration information may be used to send you information and updates regarding your order. The legal basis is Art. 6 para. 1 p. 1 lit. b GDPR.



to contact you for marketing purposes: In addition and depending on whether you have given us your consent, we may use your email address to occasionally send you company news, updates and related product or service information and special offers. If at any time you wish to unsubscribe from future emails, you will find detailed unsubscribe instructions at the bottom of each email we send you.



Please continue reading below in section 5 - the rest of this Privacy Policy contains important information that also applies to you.

## 4. Data protection for whistleblowers
### 4.1 Who is a whistleblower?
If you access a whistleblower portal for a company or organisation via one of our websites or the website of the company or organisation to which our users belong, which is usually accessible at nameofacompany.hinthub.eu, and make a whistleblowing submission there by providing personal data or by making it anonymous, you are a whistleblower.

### 4.2 What data do we collect about whistleblowers?
We explicitly point out that HintHub is not to be understood as a third party in the sense of (54) Directive (EU) 2019/1937 and thus cannot be regarded as a receiving body for reports. We therefore also exclude any commissioned data processing for users and any responsibility for the handling of hints submitted by users and the use or disclosure of the personal data of the latter.



If you submit personal data as a whistleblower, we collect the following data:

Metadata about the submission of hints: We collect, store and analyse the frequency of reports and relate this to their plausibility, in particular to prevent harmful use of our systems and to prevent harm to users.

Email address: HintHub records the e-mail address of whistleblowers if this is provided in order to be able to automatically send you further information on the processing of your hinit by a user. The legal basis is Art. 6 para. 1 p. 1 lit. b, Art. 28 para. 3 GDPR in conjunction with Directive (EU) 2019/1937 of the European Parliament and of the Council of 23 October 2019 on the protection of individuals who report infringements of Union law. 

Of course, you can make use of the opt-out at any time by following the unsubscribe instructions at the bottom of any email you receive from us.

### 4.3 How do we use the data we collect about whistleblowers?
We use data about whistleblowers in aggregate form to produce reports or benchmarks. This means that we may use your data for security and operational management purposes, to produce statistical analyses, and for research and development purposes. Please note that these analyses do not allow the identification of an individual or contain data that could help to identify them. The legal basis is Art. 6 para. 1 p. 1 lit. f GDPR, based on the legitimate interest in the further development of our service.

## 5. Data controllers and data processors
HintHub is the "controller" within the meaning of Article 4 of the GDPR for all website visitor and creator information that we collect. However, the data controller for the whistleblower data provided by whistleblowers is the user. Users decide at their own discretion or in accordance with applicable European or national laws how they handle personal data and information. If you have any questions about a submitted reports, the use of your reports or your personal data, please contact the respective user directly.

### 6. How long do we keep your data?
We will retain your data, if collected, in accordance with applicable legal retention periods. After this period, if the relevant data is no longer necessary for the performance of the contract between you and us, it will be routinely deleted.



If you are a user, when you delete your account, all your data will be deleted from our system to the extent permitted by law. Please note that this content cannot be restored once your account has been deleted. We will not be liable for any loss or damage following the deletion of your account or as a result thereof. It is your responsibility to ensure that content you need has been backed up or copied prior to deletion.



Notwithstanding the above, we retain and use your data to comply with our legal obligations (including legal retention periods), to resolve disputes or to enforce our contractual agreements. We may also retain and use your data to the extent that such data is incorporated into our statistical analyses, reports and benchmarks, and we will not incorporate your data in a form that permits or can reasonably be expected to assist in identifying an individual.

## 7. How website visitors can control their data
### 7.1 Your rights as a data subject in general
Your rights in relation to our processing of your personal data include the following:



Access: you can ask us at any time, free of charge, to confirm whether personal data relating to you is being processed; if it is, you have a right of access to that personal data (see section 7.2 below for more details).



Rectification: If you believe that your personal data that we hold or process is inaccurate or incomplete, you may request a rectification of that inaccurate or incomplete data (see section 7.2 below for more details).



Right to erasure (right to be forgotten): Under certain conditions, you can request the erasure of your personal data at any time (see section 7.3 below for more details).



Right to restriction of processing: Under certain conditions, you may request us to restrict processing (see section 7.4 for more details).



Right to object: You may object to the processing of your personal data at any time on grounds based on your particular situation (see section 7.5 for more details).



Right to data portability: You have the right to receive the personal data relating to you that you have provided to us in a structured, commonly used, machine-readable format (see section 7.6 for more details).



Withdrawal: If our processing of your personal data is based on your consent, you may withdraw your consent at any time without affecting the lawfulness of the processing carried out on the basis of the consent until withdrawal (see section 7.7 for more details).



Complaint: If you believe that our data processing measures or means violate the GDPR, you can lodge a complaint with a supervisory authority (see section 7.8 for more details).

### 7.2 Can I access and change my personal data?
Yes. You have the right to access the personal data HintHub processes about you at any time and you can also request us to rectify, erase, restrict or block this data if necessary. You can exercise these rights at any time by contacting our Privacy Support at privacy@hinthub.eu.



If you are a user, you can change your personal data by logging in and following the instructions provided under your settings on the "My Account" page and the "Plan + Billing" page, or by visiting our "Contact us" page. We recommend that you update your personal information immediately if it changes.



If you are a whistleblower, please note that your personal information is maintained and controlled exclusively by the individual user you have contacted through HintHub to submit a whistleblower. If you wish to review, correct, delete, restrict or block your personal data, please contact the relevant user(s) directly.

### 7.3 Was tun, wenn ich nicht will, dass meine personenbezogenen Daten verarbeitet werden?
If you do not want your personal data to be processed, you have the right to request that we delete the personal data concerning you without undue delay, provided that one of the following reasons applies:

The personal data has been collected for purposes or processed in another way for which it is no longer necessary;

The personal data are no longer necessary for the purposes for which they were collected or otherwise processed.

You withdraw your consent on which the processing was based pursuant to Article 6(1)(a) or Article 9(2)(a) DSGVO and there is no other legal basis for the processing.

You object to the processing pursuant to Article 21(1) of the GDPR and there are no overriding legitimate grounds for the processing, or you object to the processing pursuant to Article 21(2) of the GDPR.

The personal data have been processed unlawfully.

The erasure of the personal data is necessary for compliance with a legal obligation under Union or Member State law to which we are subject.

The personal data has been collected in relation to information society services offered in accordance with Article 8(1) of the GDPR.

If one of the above reasons applies and you wish your personal data to be deleted, please contact our data protection support at privacy@hinthub.eu. 

We will make every effort to comply with the deletion request without delay.

### 7.4 Can I restrict the processing of my data?
Yes. You can request a so-called "restriction of processing" from us if one of the following conditions applies:

You dispute the accuracy of the personal data for a period of time that allows us to verify the accuracy of the personal data;

the processing of your personal data is unlawful and you object to the erasure of the personal data and request instead the restriction of the use of the personal data;

We no longer need your personal data for the purposes of processing, but you need it to assert, exercise or defend legal claims;

You have objected to the processing pursuant to Article 21(1) GDPR as long as it has not yet been determined whether our legitimate grounds outweigh your grounds.

If one of the aforementioned conditions applies and you wish to restrict the processing of your personal data stored by us, you can contact us at any time to have the processing restricted.

### 7.5 Can I object to the processing of my personal data?
Yes. You have the right to object at any time, on grounds relating to your particular situation, to the processing of personal data concerning you which is carried out on the basis of Article 6(1)(e) or (f) of the GDPR. In this case, we will no longer process the personal data unless we can demonstrate compelling legitimate grounds pursuant to Article 21(1) DSGVO or the processing serves the purpose of asserting, exercising or defending legal claims.

If we process your personal data for the purpose of direct marketing, you have the right to object at any time to the processing of your personal data for the purpose of such marketing; this also applies to profiling (individual profiling) insofar as it is related to such direct marketing. If you object to the processing for direct marketing purposes, we will no longer process your personal data for these purposes.

If we process personal data for scientific or historical research purposes or for statistical purposes pursuant to Article 89 (1) of the GDPR, you have the right to object on grounds relating to your particular situation to the processing of personal data concerning you, unless the processing is necessary for the performance of a task carried out in the public interest.

### 7.6 Can I get my data transferred to another data controller?
Yes. You have the right to receive the personal data concerning you that you have provided to us in a structured, commonly used and machine-readable format, and you have the right to transfer this data to another controller without hindrance from us, provided that the processing is based on consent pursuant to Article 6(1)(a) or Article 9(2)(a) of the GDPR or on a contract pursuant to Article 6(1)(b) of the GDPR and the processing is carried out with the aid of automated procedures.

Please note that this transfer may be prohibited if the processing is necessary for the performance of a public interest task or the exercise of official authority vested in HintHub. When exercising your right to data portability, you also have the right to have your personal data transferred directly from us to another controller, where technically feasible.

### 7.7 Was passiert wenn ich meine Meinung ändere – kann ich meine Einwilligung widerrufen?
Yes. As stated in Chapter 1.2, you must agree to this Privacy Policy and our Terms and Conditions in order to obtain permission to use our service. However, if you no longer wish to use our service, you have the right to withdraw your consent to the processing of personal data relating to you at any time. To do this, please contact our data protection support at privacy@hinthub.eu. If you revoke your consent, you will no longer be able to use our services. Please note that the revocation does not affect the lawfulness of the processing carried out on the basis of the consent until the revocation.

### 7.8 Everything went wrong - what can I do?
If you believe that our processing of personal data concerning you is in breach of the GDPR, inadequate or you have other grounds that apply to you, you have the right to lodge a complaint with a supervisory authority in accordance with Article 77 of the GDPR.

## 8. Data security
### 8.1 Where is your data stored?
The servers on which your data and information are stored are located in Germany, a member state of the European Union, or another signatory to the Agreement on the European Economic Area.

### 8.2 How do we back up your data?
We are obliged to handle your personal data with the utmost care. To this end, we have implemented and maintain various technical and organisational measures. These measures are designed to protect your data against unauthorised loss, destruction, alteration, disclosure or access and against all unlawful forms of processing.

### 8.3 Is there a risk that my data can still be viewed by third parties?
Unfortunately, yes. Regardless of the security safeguards and precautions we take, there is always a risk that your personal information may be accessed and used by third parties as a result of its collection and transmission through the Internet. If you have any questions about the security of your personal data, please contact us.

What is the legal basis for the data processing?
When processing your personal data, HintHub will at all times act in accordance with the applicable law. We have identified the appropriate legal basis for each data processing operation. Below we explain what these mean:

Data processing operations based on Article 6(1)(a) GDPR are lawful if the data subject has given consent to the processing for one or more specific purposes.

If the processing of personal data is necessary for the performance of a contract to which you are a party or for the implementation of pre-contractual measures, our processing is based on Article 6 (1) (b) of the GDPR. Pre-contractual measures are to be understood in a broad sense and may already include surfing on our website.

If we are subject to a legal obligation that makes the processing of personal data necessary (such as the fulfilment of tax obligations), our processing is based on Article 6 (1) (c) of the GDPR.

In very rare cases, the processing of personal data by us may be necessary to protect vital interests of the data subject or another natural person; in these cases, our processing is based on Article 6 (1) (d) GDPR.

Currently, we do not carry out tasks that are in the public interest or in the exercise of official authority; however, this may take place in the future and in this case our processing is based on Article 6(1)(e) GDPR.

Finally, the processing of personal data by us may be based on Article 6(1)(f) GDPR. This legal basis is used for processing operations that are not covered by one of the above grounds, where the processing is necessary for the purposes of the legitimate interests of us or of a third party, except where such interests are overridden by the interests or fundamental rights and freedoms of the data subject which require the protection of personal data (the standard is stricter where the data subject is a child).

Where our processing of personal data is based on Article 6(1)(f) GDPR, our legitimate interests are briefly outlined; this includes, in particular, operating our website, providing our services, personalising the experience of our website visitors and meeting their individual needs (including the continuous improvement of our website and customer services), and the interest in contacting you for marketing purposes (where you have given us your consent to do so).

## 10. Does HintHub use automated decision making?
No. As a responsible company, HintHub does not use automated decision making.

## 11. Changes to this privacy policy
HintHub reserves the right to change this Privacy Policy at any time by posting the date of change on this page or elsewhere on the Site. We strongly recommend that you check this page frequently for the date of the last change. If you object to any changes to this Privacy Policy, you must stop using HintHub.

## 12. How to contact us?
Here is our contact information (in accordance with Article 4 Paragraph 7 of the European Data Protection Regulation GDRP):

HintHub GmbH, Hauptstr. 28, 15806 Zossen, Germany, privacy@hinthub.eu

## Annex 1 to the privacy policy
Technical and organisational measures
HintHub warrants that it has taken the following technical and organisational measures, insofar as HintHub can directly influence them:Measures to ensure confidentiality

### 1.1 Access control
Measures that physically prevent unauthorised persons from accessing IT systems and data processing equipment with which personal data are processed, as well as confidential files and data carriers. Description of the access control system:

Locks on entrance doors

Careful selection of cleaning staff

Controlled key allocation

Access management: authorised personnel and scope of authorisation are predefined

Measures taken by service providers from Annex 2

### 1.2 Data access control
Measures that prevent unauthorised persons from processing or using data protected under data protection law.

Description of the access control system:

System and data access are restricted to authorised users.

Users must identify themselves with username and password

All logins/logoffs are recorded

Use of a central password policy

### 1.3 Conditional access
Measures to ensure that those authorised to use the data processing procedures can only access the personal data subject to their access authorisation, so that data cannot be read, copied, modified or removed without authorisation during processing, use and storage.

Description of the access control system:

System and data access are restricted to authorised users.

Users must identify themselves with username and password

All data access is automatically recorded

A small number of system administrators

Logging of access and attempted abuse

### 1.4 Separation principle
Measures to ensure that data collected for different purposes are processed separately and are segregated from other data and systems in such a way that unplanned use of these data for other purposes is precluded.

Description of the segregation control process:

Systems allow data segregation through different software

Production and test systems are segregated from each other

Data sets are only accessible through systems that are predefined

Databases User rights are issued and managed centrally

### 1.5 Pseudonymisation
Measures that reduce the direct reference to a person during processing in such a way that an assignment to a specific data subject is only possible with the use of additional information. The additional information must be kept separate from the pseudonym by means of appropriate technical and organisational measures.

None, since processing takes place on a central server

Measures to ensure integrity

### 2.1 Passing on control
Measures to ensure that personal data cannot be read, copied, altered or removed by unauthorised persons during electronic transmission or while being transported or stored on data media, and measures to verify and identify to which bodies personal data are intended to be transmitted. Description of the transfer control:

HTTPS - Unnecessary printouts and misprints are destroyed.

No use of physical data carriers

Comprehensive logging procedures

Private data carriers of staff may not be used at work

### 2.2 Input control
Measures to ensure that it is possible to check and establish retrospectively whether and by whom personal data have been entered, modified or removed from data processing systems.Description of the input control process:Logging of all system activities and retention of the logs for at least 6 monthsUse of the central rights management for the input, modification and deletion of dataMeasures to ensure availability and resilience

### 3.1 Availability check
Measures to ensure that personal data is protected against accidental destruction or loss. Description of the availability control system:

Backups are made regularly.

Backup and recovery plan is in place.

Backup files are stored in a secure and remote location.

Localisation - data recovery is tested regularly

As well as various other measures taken by the server service providers.

### 3.2 Rapid recoverability
Measures to ensure the ability to rapidly restore the availability of and access to personal data in the event of a physical or technical incident.

Description of measures:

Data security procedures

Measures for the regular evaluation of the security of data processing

Measures to ensure data protection compliant and secure processing.

Description of review procedures:

Formalised process in the event of a data protection incident.
